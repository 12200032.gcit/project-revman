const express = require('express');
const { Pool } = require('pg');
const session = require('express-session');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const cors = require('cors');
const router = express.Router();
const pool = require("../db");

// const cookieSession = require('cookie-session');
const app = express();

// Serve static files from the "uploads" directory
router.use('/uploads', express.static('uploads'))


router.get('/getFoods', async (req, res) => {

    pool.query(`SELECT * FROM food`, (error, result) => {
      if (error) {
        throw error;
      }
      res.send(result.rows);
    });
    
  });

  router.get('/getFood', async (req, res) => {

    pool.query(`SELECT * FROM food`, (error, result) => {
      if (error) {
        throw error;
      }
      res.send(result.rows);
    });
  
  })

  router.get('/food_detail/:category/:sorting', async (req, res) => {

    const category = req.params.category;
    const sorting = req.params.sorting;
  
    if (sorting == "highest") {
      pool.query(`SELECT * FROM food WHERE category = '${category}'ORDER BY price DESC;`, (error, result) => {
        if (error) {
          throw error;
        }
        res.send(result.rows);
  
      });
    }
  
    if (sorting == "lowest") {
      pool.query(`SELECT * FROM food WHERE category = '${category}'ORDER BY price ASC;`, (error, result) => {
        if (error) {
          throw error;
        }
        res.send(result.rows);
  
      });
    }
  
  });

  module.exports = router;