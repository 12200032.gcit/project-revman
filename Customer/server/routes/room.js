const express = require('express');
const { Pool } = require('pg');
const session = require('express-session');

const bcrypt = require('bcrypt');
const cors = require('cors');
const router = express.Router();
const pool = require("../db");

// const cookieSession = require('cookie-session');
const app = express();

// Serve static files from the "uploads" directory
router.use('/uploads', express.static('uploads'))


router.get('/getRooms', async (req, res) => {

  pool.query(`SELECT * FROM room`, (error, result) => {
    if (error) {
      throw error;
    }
    res.send(result.rows);
  });

});

router.get('/room_detail/:category/:sorting', async (req, res) => {

  const category = req.params.category;
  const sorting = req.params.sorting;

  if (sorting == "highest") {
    pool.query(`SELECT * FROM room WHERE category = '${category}'ORDER BY price DESC;`, (error, result) => {
      if (error) {
        throw error;
      }
      res.send(result.rows);

    });
  }

  if (sorting == "lowest") {
    pool.query(`SELECT * FROM room WHERE category = '${category}'ORDER BY price ASC;`, (error, result) => {
      if (error) {
        throw error;
      }
      res.send(result.rows);

    });
  }

});


router.post('/book_room', async (req, res) => {

  try {
    const { room_code, name, phone_number, email, check_in, check_out, message } = req.body;

    // post to database
    const { rows } = await pool.query(
      'INSERT INTO customer (room_code,name,phone_number,email,check_in,check_out,message,booking_time,status) VALUES ($1, $2, $3, $4, $5, $6 ,$7,CURRENT_TIMESTAMP,$8) RETURNING *',
      [room_code, name, phone_number, email, check_in, check_out, message,"true"]

    );
    await pool.query(
      'UPDATE room SET status = $1 WHERE room_code = $2', ['true', room_code]
    );

    res.status(201).json(rows[0]);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server Error' });
  }
});

module.exports = router;