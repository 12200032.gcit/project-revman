import React from 'react';

export default function Home() {

 

  return (
    <>
  <div>
  
  <strong>
   
  
    <header>
              <div className="header-area header-sticky">
                <div className="main-header">
                  <div className="container">
                    <div className="row align-items-center">
                      {/* logo */}
                      <div className="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4">
                        <div className="img-fluid">
                          <a href="index.html">
                            <img src="assets/img/45.png" alt="logo" style={{maxWidth:' 50%'}} className="img-fluid"/>
                          </a>
                        </div>
                      </div>
                      {/* hamburger icon */}
                    
                      {/* mobile menu */}
                      <div className="col-12 d-block d-lg-none">
                        
                      </div>
                      <div className="col-xl-8 col-lg-8">
                        <div className="main-menu f-right d-none d-lg-block">
                          <nav>
                            <ul id="navigation" style={{fontWeight: 'bold', display: 'flex', alignItems: 'center'}}>
                              <li style={{marginRight: '20px'}}>
                                <a href="index.html">Home</a>
                              </li>
                              <li style={{marginRight: '20px'}}>
                                <a href="about.html">Food</a>
                              </li>
                              <li style={{marginRight: '20px'}}>
                                <a href="services.html">Room</a>
                              </li>
                              <li style={{marginRight: '20px'}}>
                                <a href="blog.html">About us</a>
                              </li>
                              <li>
                                <a href="contact.html">Contact us</a>
                              </li>
                            </ul>
                          </nav>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </header>
 <main>
 <div className="slider-area">
      {/* Mobile Menu */}
      <div className="slider-active dot-style">
        <div
          className="single-slider hero-overly slider-height d-flex align-items-center"
          style={{ backgroundImage: "url('assets/img/0.jpg')" }}
        >
          <div>
  {/* content goes here */}
</div>

          <div className="container">
            <div className="row justify-content-center text-center">
              <div className="col-xl-9">
                <div className="h1-slider-caption">
                  <h1 data-animation="fadeInUp" data-delay=".4s">
                    Welcome to Hotel
                  </h1>
                  <h3 data-animation="fadeInDown" data-delay=".4s">
                    REVMEN
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  {/* Booking Room Start*/}
  <div className="booking-area">
    <div className="container">
      <div className="row ">
        <div className="col-12">
          <form action>
            <div className="booking-wrap d-flex justify-content-between align-items-center">
              {/* select in date */}
              <div className="single-select-box mb-30">
                {/* select out date */}
                <div className="boking-tittle">
                  <span> Check In Date:</span>
                </div>
                <div className="boking-datepicker">
                  <input id="datepicker1" placeholder="10/12/2020" />
                </div>
              </div>
              {/* Single Select Box */}
              <div className="single-select-box  mb-30">
                {/* select out date */}
                <div className="boking-tittle">
                  <span>Check OutDate:</span>
                </div>
                <div className="boking-datepicker">
                  <input id="datepicker2" placeholder="12/12/2020" />
                </div>
              </div>
             
              <div className="single-select-box  mb-30">
                <a href="1" className="btn select-btn mt-2" style={{width:'230px'}}>
                  Check Availability 
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
   
  <section className="make-customer-area customar-padding fix" >
      <div className="container-fluid p-0">
        <div className="row">
         
          <div className=" col-xl-12 col-lg-12">
            <div className="customer-caption text-center">
              <h3>Revman hotel well comes you</h3>
              <div className="caption-details">
               <p >In the midst of the hustle and bustle Hotel revman Phuentsholing offers a quaint setting. Combining classical<br /> architecture 
                with modern comfort the Druk Hotel offers beautifully appointed rooms including junior suites <br /> and presidential suite Hotel Druk plays host. 
              </p>           
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    {
  /* Room Start */
}

<section className="room-area make-customer-area customar-padding fix" style={{backgroundColor: "#D9D9D982"}}>
  <div className="container ">
    <div className="row justify-content-center">
      <div className="col-xl-8">
        {/*font-back-tittle  */}
        <div className="font-back-tittle mb-45 mt-3">
          <div className="archivment-front">
            <h3>ROOM Category</h3>
          </div>
       
        </div>
      </div>
    </div>
    <div className="row">
      <div className="col-xl-4 col-lg-4 col-md-4 sm-12">
        {/* Single Room */}
        <div className="single-room mb-50">
          <div className="room-img">
            <a href="rooms.html">
              <img src="assets/img/22.png" alt='23' />
            </a>
          </div>
          <div className="room-caption">
            <h3 className='text-center'>
              <a href="rooms.html">Gold Suite Room</a>
            </h3>
            <p className='text-center'>Modern in design, the warm grain of the American black walnut furnishings enriches the airy guest rooms.</p>
            <div className="per-night">
              <span>
            
                <div className="single-select-box pt-4 mb-30 text-center">
                        <a href="1" className="btn select-btn">View</a>
                   </div>
               
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4 col-lg-4 col-md-4 sm-12">
        {/* Single Room */}
        <div className="single-room mb-50">
          <div className="room-img">
            <a href="rooms.html">
              <img src="assets/img/25.jpg" alt='23' />
            </a>
          </div>
          <div className="room-caption">
            <h3 className='text-center'>
              <a href="rooms.html">RED DELUXE ROOMS</a>
            </h3>
            <p className='text-center'>Modern in design, the warm grain of the American black walnut furnishings enriches the airy guest rooms.</p>
            <div className="per-night">
              <span>
            
                <div className="single-select-box pt-4 mb-30 text-center">
                        <a href="1" className="btn select-btn">View</a>
                   </div>
               
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4 col-lg-4 col-md-4 sm-12">
        {/* Single Room */}
        <div className="single-room mb-50">
          <div className="room-img">
            <a href="rooms.html">
              <img src="assets/img/22.png" alt='23' />
            </a>
          </div>
          <div className="room-caption">
            <h3 className='text-center'>
              <a href="rooms.html">grey DELUXE ROOMS</a>
            </h3>
            <p className='text-center'>Modern in design, the warm grain of the American black walnut furnishings enriches the airy guest rooms.</p>
            <div className="per-night">
              <span>
            
                <div className="single-select-box pt-4 mb-30 text-center">
                        <a href="1" className="btn select-btn">View</a>
                   </div>
               
              </span>
            </div>
          </div>
        </div>
      </div>
     
     
    </div>
    <div className="row justify-content-center">
      <div className="room-btn">
        <a href="7" className="btn mb-4 view-btn1">
          View all Room  <i className="ti-angle-right" />{' '}
        </a>
      </div>
    </div>
  </div>
</section>;
{
  /* Room End */
}


<section className="make-customer-area customar-padding fix">
     
        <div className="container-fluid p-0 ml-4">
          <div className="row mr-3 pl-5">
          
            <div className="col-xl-5 col-lg-6">
              <div className="customer-img mb-120">
              
                <img src="assets/img/25.jpg" className="customar-img1" alt="" />
              
               
              </div>
            </div>
           
            <div className=" col-xl-4 col-lg-4">
             
              <div className="customer-caption">
                <h2>TOP-Rated ROOMs </h2>
                <h2>Red deluxe room</h2>
                <div className="caption-details">
                  <p className="pera-dtails">Lorem ipsum dolor sit amet, consectetur adipisic- ing elit, sed do eiusmod tempor inc. </p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. </p>
                  <a href="2" className="btn more-btn1 w-75">View more<i className="ti-angle-right" /> </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="make-customer-area customar-padding fix" style={{backgroundColor: '#D9D9D982'}}>
         <div className="container-fluid p-0 ml-4">
          <div className="row mr-3 pl-5"> 
            <div className=" col-xl-4 sm-12 col-lg-4">
             
              <div className="customer-caption">
              <h2>TOP-Rated ROOMs </h2>
          <h2>Green deluxe rooms</h2>
                <div className="caption-details">
                  <p className="pera-dtails">Lorem ipsum dolor sit amet, consectetur adipisic- ing elit, sed do eiusmod tempor inc. </p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. </p>
                  <a href="2" className="btn more-btn1 mb-5 w-75">View more<i className="ti-angle-right" /> </a>
                </div>
              </div>
            </div>
              <div className="col-xl-5 col-lg-6">
              <div className="customer-img mb-120"> 
                <img src="assets/img/20.jpg" className="customar-img1" alt="" />     
              </div>
            </div> 
          </div>
        </div>
</section>



<section className="room-area  make-customer-area customar-padding fix">
  <div className="container ">
    <div className="row justify-content-center  ">
      <div className="col-xl-8">
        {/*font-back-tittle  */}
        <div className="font-back-tittle mb-45 mt-3">
          <div className="archivment-front">
            <h3>FOOD Category</h3>
          </div>
         
        </div>
      </div>
    </div>
    <div className="row">
      <div className="col-xl-4 col-lg-4 col-md-4 sm-12 ">
        {/* Single Room */}
        <div className="single-room mb-50" style={{backgroundColor: '#D9D9D982'}}>
          <div className="room-img">
            <a href="rooms.html">
              <img src="assets/img/veg2.jfif" alt='23' />
            </a>
          </div>
          <div className="room-caption">
            <h3 className='text-center'>
              <a href="rooms.html">vegetable</a>
            </h3>
            <p className='text-center'>Modern in design, the warm grain of the American black walnut furnishings enriches the airy guest rooms.</p>
            <div className="per-night">
              <span>
            
                <div className="single-select-box pt-4 mb-30 text-center">
                        <a href="1" className="btn select-btn">View </a>
                   </div>
               
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4 col-lg-4 col-md-4 sm-12">
        {/* Single Room */}
        <div className="single-room mb-50" style={{backgroundColor: '#D9D9D982'}}>
          <div className="room-img">
            <a href="rooms.html">
              <img src="assets/img/meat.jpg" alt='23' />
            </a>
          </div>
          <div className="room-caption">
            <h3 className='text-center'>
              <a href="rooms.html">Meat and poultry</a>
            </h3>
            <p className='text-center'>Modern in design, the warm grain of the American black walnut furnishings enriches the airy guest rooms.</p>
            <div className="per-night">
              <span>
            
                <div className="single-select-box pt-4 mb-30 text-center">
                        <a href="1" className="btn select-btn">View</a>
                   </div>
               
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4 col-lg-4 col-md-4 sm-12">
        {/* Single Room */}
        <div className="single-room mb-50" style={{backgroundColor: '#D9D9D982'}}>
          <div className="room-img">
            <a href="rooms.html">
              <img src="assets/img/16.jpg" alt='23' />
            </a>
          </div>
          <div className="room-caption">
            <h3 className='text-center'>
              <a href="rooms.html">Fish and seafood</a>
            </h3>
            <p className='text-center'>Modern in design, the warm grain of the American black walnut furnishings enriches the airy guest rooms.</p>
            <div className="per-night">
              <span>
            
                <div className="single-select-box pt-4 mb-30 text-center">
                        <a href="1" className="btn select-btn">View</a>
                   </div>
               
              </span>
            </div>
          </div>
        </div>
      </div>
     
     
    </div>
    <div className="row justify-content-center">
      <div className="room-btn">
        <a href="7" className="btn mb-4 view-btn1">
          View All Food items <i className="ti-angle-right" />{' '}
        </a>
      </div>
    </div>
  </div>
</section>;
{
  /* Room End */
}
<section className="make-customer-area customar-padding fix" style={{backgroundColor: '#D9D9D982'}}>
     
        <div className="container-fluid p-0 ml-4">
          <div className="row mr-3 pl-5">
          
            <div className="col-xl-5 col-lg-6">
              <div className="customer-img mb-120">
              
                <img src="assets/img/v4.png" className="customar-img1" alt="" />
              
               
              </div>
            </div>
           
            <div className=" col-xl-4 sm-12 col-lg-4" >
             
              <div className="customer-caption">
                <h2>TOP-Rated Foods  </h2>
                <h2>jasha Maroo</h2>
                <div className="caption-details">
                  <p className="pera-dtails">Lorem ipsum dolor sit amet, consectetur adipisic- ing elit, sed do eiusmod tempor inc. </p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. </p>
                  <a href="2" className="btn more-btn1 w-75">View more<i className="ti-angle-right" /> </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> 
   <section className="make-customer-area customar-padding fix" >
         <div className="container-fluid p-0 ml-4">
          <div className="row mr-3 pl-5">
          
           
           
            <div className=" col-xl-4 sm-12 col-lg-4">
             
              <div className="customer-caption">
                <h2>TOP-Rated Foods  </h2>
                <h2>Sikam pa</h2>
                <div className="caption-details">
                  <p className="pera-dtails">Lorem ipsum dolor sit amet, consectetur adipisic- ing elit, sed do eiusmod tempor inc. </p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. </p>
                  <a href="2" className="btn more-btn1 mb-5 w-75">View more<i className="ti-angle-right" /> </a>
                </div>
              </div>
            </div>
              <div className="col-xl-5 col-lg-6">
              <div className="customer-img mb-120">
                <img src="assets/img/sikampa.jpg" className="customar-img1" alt="sikampa" /> 
              </div>
            </div>
             
          </div>
        </div>
</section>
 </main>

 <footer>
  {/* Footer Start*/}
<div className="footer-area footer-padding" style={{background: '#445C4C', color: 'white'}}>
  <div className="container">
    <div className="row d-flex justify-content-between">
 
  {/* logo*/}
    <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3">
  <div className="single-footer-caption mb-30">
    <div className="d-flex align-items-center justify-content-center mb-5 mr-5">
      <a href="index.html">
      <img src="assets/img/1.png" alt="logo" class="img-fluid rounded-circle logo-img logo-img-lg"/>
     
      </a>
    </div>
  </div>
</div>
    {/*nav link*/}

      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3">
        <div className="single-footer-caption mb-30">
          <div className="footer-tittle ">
            <ul style={{paddingLeft: '0'}}>
              <li>
                <a class="text-white"href="2">Home</a>
              </li>
              <li>
                <a href="2" class="text-white">Room</a>
              </li>
              <li>
                <a href="2" class="text-white">Food</a>
              </li>
              <li>
                <a href="2" class="text-white">About us</a>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3">
        <div className="single-footer-caption mb-30">
          <div className="footer-tittle">
            <h4>Hours</h4>
            <ul style={{paddingLeft: '0'}}>
              <li>
                <p>Mon -Fri: 8am -5pm</p>
              </li>
              <li>
                <p>Sat-Sun: 10am-3pm</p>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3">
        <div className="single-footer-caption mb-30">
          <div className="footer-tittle">
            <h4>Contact us</h4>
            <ul style={{paddingLeft: '0'}}>
              <li>
                <p>Phone: 17261998</p>
              </li>
              <li>
                <a href="2">reservations@hotelriver.com</a>
              </li>
            </ul>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

 
</footer>;


</strong>
</div> 
   </>
  )
}
