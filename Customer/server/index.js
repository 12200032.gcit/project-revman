const express = require("express");
const app = express();
const cors = require("cors");

//middleware

app.use(cors());
app.use(express.json());


//room routes
app.use("/room", require("./routes/room"));

//food routes
app.use("/food", require("./routes/food"));

//routes
app.listen(5001, () => {
  console.log(`Server is starting on port 5001`);
});