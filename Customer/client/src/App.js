import React from "react";
import Home from "./components/Home";
import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom";
import FackHome from "./components/FackHome";
import "./App.css";
import Try from "./components/Try";
import Room from "./components/Room";
import ViewRoom from "./components/ViewRoom";
import Contact from "./components/Contact";
import Food from "./components/Food";
import ViewFood from "./components/ViewFood";
import FoodCart from "./components/FoodCart";

function App() {
  return (
    <Router>
      <Routes>
        <Route
          path="/"
          element={<Home/>}
        />
        <Route
          path="/room"
          element={<Room/>}
        />
         <Route
          path="/view_room"
          element={<ViewRoom/>}
        />
         <Route
          path="/contact"
          element={<Contact/>}
        />
         <Route
          path="/food"
          element={<Food/>}
        />
         <Route
          path="/view_food"
          element={<ViewFood/>}
        />
      </Routes>
    </Router>
  );
}

export default App;
