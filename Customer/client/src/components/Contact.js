import React from 'react'
import Header from './Header';
import Footer from './Footer';

function Contact() {
  return (
    <>

      <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
          <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
              <strong> REVMAN</strong>
            </div>
          </div>
        </div>
      </div>

      <Header />

      <main>
        {/* <!-- slider Area Start--> */}
        <div className="slider-area" style={{ height: "auto" }}>
          {/* Mobile Menu */}
          <div className="slider-active dot-style">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

              <div class="carousel-inner">
                <div class="carousel-item active">
                  <div className="slider-active">
                    <div
                      className="single-slider hero-overly slider-height2 d-flex align-items-center"
                      style={{ backgroundImage: "url('assets/img/hero/contact_hero.jpg')" }}
                    >
                      <div class="container">
                        <div class="row ">
                          <div class="col-md-11 offset-xl-1 offset-lg-1 offset-md-1">
                            <div class="hero-caption my-element">
                              <span>contact</span>
                              <h2>Contact Us</h2>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* <!-- ================ contact section start ================= --> */}
        <section class="contact-section">
          <div class="container">
            <div class="d-none d-sm-block mb-5 pb-4"></div>

            <div class="row">
              <div class="col-12">
                <h2 class="contact-title">Get in Touch</h2>
              </div>
              <div class="col-lg-8">
                <form class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                  <div class="row">

                    <div class="col-12">
                      <div class="form-group">
                        <input class="form-control" name="subject" id="subject" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'" placeholder="Enter Subject" />
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" placeholder="Enter your name" />
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input class="form-control valid" name="number" id="name" type="number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your phone number'" placeholder="Enter your phone number" />
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" placeholder="Email" />
                      </div>
                    </div>

                    <div class="col-12">
                      <div class="form-group">
                        <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'" placeholder=" Enter Message"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-group mt-3">
                    <button type="submit" class="button button-contactForm boxed-btn">Send</button>
                  </div>
                </form>
              </div>
              <div class="col-lg-3 offset-lg-1">
                <div class="media contact-info">
                  <span class="contact-info__icon"><i class="ti-home"></i></span>
                  <div class="media-body">
                    <h3>Clock Tower Square,</h3>
                    <p> Wogzin Iham, Thimphu 1178</p>
                  </div>
                </div>
                <div class="media contact-info">
                  <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                  <div class="media-body">
                    <h3>+975 1726 8895</h3>
                    <p>Mon to Fri 8am to 6pm</p>
                  </div>
                </div>
                <div class="media contact-info">
                  <span class="contact-info__icon"><i class="ti-email"></i></span>
                  <div class="media-body">
                    <h3>revman@gorupteam.com</h3>
                    <p>Send us your query anytime!</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* <!-- ================ contact section end ================= --> */}
      </main>

      <Footer />
    </>
  )
}

export default Contact;