import React from 'react'

export default function Footer() {
  return (
    <>
      <footer>
        {/* <!-- Footer Start--> */}
        <div class="container-fluid footer-area black-bg footer-padding">
          <div>
            <div class="row d-flex justify-content-between">
              <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12">
                <div class="single-footer-caption mb-30">
                  {/* <!-- logo --> */}
                  <div class="footer-logo">
                    <a href="#"><img src="assets/img/logo/black-logo.jpg" alt="logo" style={{ width: "300px" }} /></a>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 custom-col">
                <div class="single-footer-caption mb-30">
                  <div class="footer-tittle">
                    <h4>Quick Links</h4>
                    <ul>
                      <li><a href="#">About Revman</a></li>
                      <li><a href="#">Our Best Rooms</a></li>
                      <li><a href="#">Our Best food</a></li>
                      <li><a href="#">Our Photo Gellary</a></li>

                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 custom-col">
                <div class="single-footer-caption mb-30">
                  <div class="footer-tittle">
                    <h4>Contact</h4>
                    <ul>
                      <li><a href="#">Tel: 975 5667 889</a></li>
                      <li><a href="#">revman@gorupteam.com</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-md-12  col-sm-12 custom-col">
                <div class="single-footer-caption mb-30">
                  <div class="footer-tittle">
                    <h4>Our Location</h4>
                    <ul>
                      <li><a href="#">Clock Tower Square,</a></li>
                      <li><a href="#"> Wogzin Iham, Thimphu 1178</a></li>
                    </ul>
                    {/* <!-- Form --> */}
                    <div class="footer-form" >
                      <div id="mc_embed_signup">
                        <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                          method="get" class="subscribe_form relative mail_part">
                          <input type="email" name="email" id="newsletter-form-email" placeholder="Email Address"
                            class="placeholder hide-on-focus" onfocus="this.placeholder = ''"
                            onblur="this.placeholder = ' Email Address '" />
                          <div class="form-icon">
                            <button type="submit" name="submit" id="newsletter-submit"
                              class="email_icon newsletter-submit button-contactForm"><img src="assets/img/logo/form-iocn.jpg" alt="" /></button>
                          </div>
                          <div class="mt-10 info"></div>
                        </form>
                      </div>
                    </div>

                  </div>

                </div>
              </div>

            </div>

          </div>
          <div class="footer-pera text-center">
            <p>
              Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This website is just for a learning <i class="ti-heart" aria-hidden="true"> purposess </i> by <a href="#" target="_blank">Revman Group Team</a>
            </p>
          </div>
        </div>
        {/* <!-- Footer End--> */}
      </footer>

    </>
  )
}
