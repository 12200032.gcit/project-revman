import React, { useEffect, useState, useRef } from "react";
import axios from 'axios';
import Header from './Header';
import Footer from './Footer';
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import OwlCarousel from "react-owl-carousel";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";

function Room() {

  const rating = 3.5;

  const [activeCategory, setActiveCategory] = useState("single");
  const [activeSorting, setActiveSorting] = useState("highest");

  const handleCategoryClick = (category, sorting) => {
    setActiveCategory(category);
    setActiveSorting(sorting);
  };

  const fullStars = Math.floor(rating);
  const halfStars = Math.ceil(rating - fullStars);
  const emptyStars = 6 - fullStars - halfStars;

  // get room detail base on category

  const [data, setData] = useState([]);
  // handle category

  useEffect(() => {
    axios.get(`http://localhost:5001/room/room_detail/${activeCategory}/${activeSorting}`)
      .then(res => {
        setData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [data], [activeCategory]);


  const [carouselState, setCarouselState] = useState({
    responsive: {
      500: {
        items: 1,
        nav: true
      },
      900: {
        items: 2,
        nav: false
      },
      1300: {
        items: 3,
        loop: true,
      }
    },
    dot: true,
    loop: true,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: false,
  });

  const [roomData, setRoomData] = useState([]);

  useEffect(() => {
    axios.get(`http://localhost:5001/room/getRooms`)
      .then(res => {
        setRoomData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [roomData]);

  // find the number of room in each category

  let count_single = 0;
  let count_double = 0;
  let count_triple = 0;
  let count_quad = 0;
  roomData.map((item, index) => {

    if (item.category === "single") {
      count_single++;
    }

    if (item.category === "double") {
      count_double++;
    }

    if (item.category === "triple") {
      count_triple++;
    }

    if (item.category === "quad") {
      count_quad++;
    }
  });

  // room booking form
  const [room_code, setRoomCode] = useState();
  const nameRef = useRef(null);
  const numberRef = useRef(null);
  const emailRef = useRef(null);
  const checkInDateRef = useRef(null);
  const checkOutDateRef = useRef(null);
  const messageRef = useRef(null);

  function settingRoomCode(room_code) {
    setRoomCode(room_code);
  }



  const roomBooking = async () => {
    // Send form data to server
    try {

      const formData = new FormData(); // create a new FormData object
      formData.append('room_code', room_code);
      formData.append('name', nameRef.current.value);
      formData.append('phone_number', numberRef.current.value);
      formData.append('email', emailRef.current.value);
      formData.append('check_in', checkInDateRef.current.value);
      formData.append('check_out', checkOutDateRef.current.value);
      formData.append('message', messageRef.current.value);
      // make the API call using axios
      // const response = await axios.post('http://localhost:5001/room/book_room', formData, {
      //   headers: {
      //     'Content-Type': 'multipart/form-data'
      //   }
      // });
      const res = await fetch('http://localhost:5001/room/book_room', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ room_code: room_code, name: nameRef.current.value, phone_number: numberRef.current.value, email: emailRef.current.value, check_in: checkInDateRef.current.value, check_out: checkOutDateRef.current.value, message: messageRef.current.value })
      })

    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <div style={{ overflow: "hidden" }}>


        {/* header start */}
        <Header />
        {/* header end */}


        <div id="preloader-active">
          <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
              <div class="preloader-circle"></div>
              <div class="preloader-img pere-text">
                <strong> REVMAN</strong>
              </div>
            </div>
          </div>
        </div>


        <main>
          {/* <!-- slider Area Start--> */}
          <div className="slider-area" style={{ height: "auto" }}>
            {/* Mobile Menu */}
            <div className="slider-active dot-style">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <div className="slider-active">
                      <div
                        className="single-slider hero-overly slider-height2 d-flex align-items-center"
                        style={{ backgroundImage: "url('assets/img/hero/roomspage_hero.jpg')" }}
                      >
                        <div class="container">
                          <div class="row ">
                            <div class="col-md-11 offset-xl-1 offset-lg-1 offset-md-1">
                              <div class="hero-caption my-element">
                                <span>Rooms</span>
                                <h2>Our Rooms</h2>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="carousel-caption d-none d-md-block my-element">
                        <h3 style={{ color: "#fff" }}>Revman Hotel's rooms, set amidst the serene landscapes of Bhutan, exude warmth and comfort. </h3>
                      </div>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <div className="slider-active ">
                      <div
                        className="single-slider hero-overly slider-height2 d-flex align-items-center"
                        style={{ backgroundImage: "url('assets/img/0.jpg')" }}
                      >
                        <div class="container">
                          <div class="row ">
                            <div class="col-md-11 offset-xl-1 offset-lg-1 offset-md-1">
                              <div class="hero-caption my-element">
                                <span>Rooms</span>
                                <h2>Our Rooms</h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="carousel-caption d-none d-md-block my-element">
                          <h3 style={{ color: "#fff" }}>If you're looking for a peaceful retreat in Bhutan, Revman Hotel's rooms are an ideal choice. </h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <div className="slider-active">
                      <div
                        className="single-slider hero-overly slider-height2 d-flex align-items-center"
                        style={{ backgroundImage: "url('assets/img/0.jpg')" }}
                      >
                        <div class="container">
                          <div class="row ">
                            <div class="col-md-11 offset-xl-1 offset-lg-1 offset-md-1">
                              <div class="hero-caption my-element">
                                <span>Rooms</span>
                                <h2>Our Rooms</h2>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                    <div class="carousel-caption d-none d-md-block my-element">
                      <h3 style={{ color: "#fff" }}>Revman Hotel's rooms are the epitome of luxury and comfort, set amidst the natural beauty of Bhutan. </h3>
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div >
          </div >

          {/* <!-- slider Area End--> */}


          <section className="room-area r-padding1">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-xl-8">
                  {/*font-back-tittle  */}
                  <div className="font-back-tittle">
                    <div className="archivment-front">
                      <h3>Our Rooms</h3>
                    </div>
                    <h3 className="archivment-back">Our Rooms</h3>
                  </div>
                </div>
              </div>

            </div>
          </section>

          <section class="section-padding">
            <div class="">
              <div class="row">
                <div class="col-xl-9 col-lg-12 mb-5 col-md-0 custom-responsive" >
                  <div class="blog_left_sidebar">
                    {/* <!-- Room Start --> */}
                    <section class="room-area">
                      <div class="row">
                        {/* room category start*/}
                        {data.map((item, index) => (
                          <div class="col-xl-4 col-lg-6 col-md-6 p-0">
                            <div className="room-design">
                              <div className="room-img">
                                <a href="./view_room"><img src={`http://localhost:5000/room/${item.main_image}`} className="image-fluid" alt='room_image' style={{ borderTopLeftRadius: "5px", borderTopRightRadius: "5px" }} /></a>
                              </div>
                              <div className="room-caption">
                                <h3 className='text-center'>
                                  <a href="./view_room">{item.room_title}</a>

                                </h3>
                                <div className="star-rating pt-3">
                                  {[...Array(fullStars)].map((_, i) => (
                                    <i key={i} className="fas fa-star"></i>
                                  ))}
                                  {[...Array(halfStars)].map((_, i) => (
                                    <i key={i} className="fas fa-star-half-alt"></i>
                                  ))}
                                  {[...Array(emptyStars)].map((_, i) => (
                                    <i key={i} className="far fa-star"></i>
                                  ))}
                                </div>
                                <div class="per-night pb-4 pt-2 d-flex ">
                                  <div className='mr-auto'>
                                    <span>Adult : {item.capacity_adult}</span><span>Child: {item.capacity_child}</span>
                                  </div>

                                  <span><u>BTN</u>{item.price} <span>/ par night</span></span>
                                </div>
                                <div className='text-center pt-2'>
                                  <a><Link to="/view_room" state={item} class="genric-btn warning-border radius">View</Link></a>
                                  <a class="genric-btn warning radius" data-bs-toggle="modal" onClick={() => settingRoomCode(item.room_code)} data-bs-target="#exampleModal"  >Book</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                        {/* room category end */}

                      </div>
                    </section>
                    {/* <!-- Room End --> */}

                    <nav class="blog-pagination justify-content-center d-flex">
                      <ul class="pagination">
                        <li class="page-item">
                          <a href="#" class="page-link" aria-label="Previous">
                            <i class="ti-angle-left"></i>
                          </a>
                        </li>
                        <li class="page-item ">
                          <a href="#" class="page-link">1</a>
                        </li>
                        <li class="page-item active">
                          <a href="#" class="page-link">2</a>
                        </li>
                        <li class="page-item">
                          <a href="#" class="page-link" aria-label="Next">
                            <i class="ti-angle-right"></i>
                          </a>
                        </li>
                      </ul>
                    </nav>
                  </div>
                </div>

                {/* side bar start*/}
                <div className="col-xl-3 col-lg-6 col-md-6">
                  <div className="blog_right_sidebar">
                    <aside className="single_sidebar_widget search_widget">
                      <form action="#">
                        <div className="form-group">
                          <div className="input-group mb-4">
                            <input
                              type="text"
                              className="form-control"
                              style={{ fontSize: "20px" }}
                              placeholder="Search Rooms"

                            />
                            <div className="input-group-append">
                              <button className="btn" type="button" style={{ fontSize: "20px" }}>
                                <i className="ti-search"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                        <button className="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" style={{ fontSize: "20px", height: "70px" }} type="submit">
                          Search
                        </button>
                      </form>
                    </aside>

                    <aside className="single_sidebar_widget post_category_widget">
                      <h4 className="widget_title">Category</h4>
                      <ul className="list cat-list">
                        <li>
                          <a style={{ cursor: "pointer" }} className={`d-flex ${activeCategory === "single" ? "active" : ""}`} onClick={() => handleCategoryClick("single", activeSorting)}>
                            <p>Single Room</p>
                            <p>({count_single})</p>
                          </a>
                        </li>
                        <li>
                          <a style={{ cursor: "pointer" }} className={`d-flex ${activeCategory === "double" ? "active" : ""}`} onClick={() => handleCategoryClick("double", activeSorting)}>
                            <p>Double Room</p>
                            <p>({count_double})</p>
                          </a>
                        </li>
                        <li>
                          <a style={{ cursor: "pointer" }} className={`d-flex ${activeCategory === "triple" ? "active" : ""}`} onClick={() => handleCategoryClick("triple", activeSorting)}>
                            <p>Triple Room</p>
                            <p>({count_triple})</p>
                          </a>
                        </li>
                        <li>
                          <a style={{ cursor: "pointer" }} className={`d-flex ${activeCategory === "quad" ? "active" : ""}`} onClick={() => handleCategoryClick("quad", activeSorting)}>
                            <p>Quadruple Room</p>
                            <p>({count_quad})</p>
                          </a>
                        </li>
                      </ul>
                    </aside>

                    <aside className="single_sidebar_widget post_category_widget">
                      <h4 className="widget_title">Sort by price</h4>
                      <ul className="list cat-list" style={{ cursor: "pointer" }}>
                        <li>
                          <a style={{ cursor: "pointer" }} className={`d-flex ${activeSorting === "highest" ? "active" : ""}`} onClick={() => handleCategoryClick(activeCategory, "highest")}>
                            <p>Highest Price</p>
                          </a>
                        </li>
                        <li>
                          <a style={{ cursor: "pointer" }} className={`d-flex ${activeSorting === "lowest" ? "active" : ""}`} onClick={() => handleCategoryClick(activeCategory, "lowest")}>
                            <p>Lowest Price</p>
                          </a>
                        </li>
                      </ul>
                    </aside>

                  </div>
                </div>
              </div>

              {/* booking form */}

              <div class="modal fade popup_form" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <form className="booking-form">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">BOOKING FORM</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        <label>
                          Name:
                          <input placeholder="ENTER YOUR NAME" type="text" ref={nameRef} required />
                        </label>
                        <label>
                          Phone Number:
                          <input placeholder="ENTER YOUR PHONE NUMBER" type="number" ref={numberRef} required />
                        </label>
                        <label>
                          Email:
                          <input placeholder="EMTER EMAIL" type="email" ref={emailRef} required />
                        </label>
                        <label>
                          Check-in date:
                          <input type="datetime-local" ref={checkInDateRef} required />
                        </label>
                        <label>
                          Check-out date:
                          <input type="datetime-local" ref={checkOutDateRef} required />
                        </label>
                        <label>
                          Message:
                          <textarea placeholder="LEAVE MESSAGE FOR US" type="text" ref={messageRef} required />
                        </label>

                      </div>
                      <div class="modal-footer">
                        <a href="#" data-bs-dismiss="modal" class="genric-btn danger radius">Cancel</a>
                        <button type="submit" onClick={roomBooking} class="btn btn-primary">Confirm</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

            </div>


          </section >
          <section className="gallery" style={{ marginLeft: "-20px", marginBottom: "-20px", marginRight: "-20px" }}>
            <OwlCarousel className="owl-theme" {...carouselState}>

              {data.map((item, index) => (
                <img className="imag-fluid" src={`http://localhost:5000/room/${item.main_image}`} alt="" />
              ))}
            </OwlCarousel>

          </section>
        </main >


        {/* footer start */}
        < Footer />
        {/* footer end */}
      </div >
      {/* <!-- Modal --> */}

    </>
  )
}


export default Room