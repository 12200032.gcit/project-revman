import React, { useEffect, useState, useRef } from "react";
import { SlideshowLightbox } from 'lightbox.js-react'
import 'lightbox.js-react/dist/index.css'
import Header from './Header';
import Footer from './Footer';
import { useLocation } from "react-router-dom";

export default function ViewRoom() {

    const location = useLocation();
    const item = location.state;


    // room booking form
    // room booking form
    const [room_code, setRoomCode] = useState();
    const nameRef = useRef(null);
    const numberRef = useRef(null);
    const emailRef = useRef(null);
    const checkInDateRef = useRef(null);
    const checkOutDateRef = useRef(null);
    const messageRef = useRef(null);

    function settingRoomCode(room_code) {
        setRoomCode(room_code);
    }



    const roomBooking = async () => {
        // Send form data to server
        try {

            const formData = new FormData(); // create a new FormData object
            formData.append('room_code', room_code);
            formData.append('name', nameRef.current.value);
            formData.append('phone_number', numberRef.current.value);
            formData.append('email', emailRef.current.value);
            formData.append('check_in', checkInDateRef.current.value);
            formData.append('check_out', checkOutDateRef.current.value);
            formData.append('message', messageRef.current.value);
            // make the API call using axios
            // const response = await axios.post('http://localhost:5001/room/book_room', formData, {
            //   headers: {
            //     'Content-Type': 'multipart/form-data'
            //   }
            // });
            const res = await fetch('http://localhost:5001/room/book_room', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ room_code: room_code, name: nameRef.current.value, phone_number: numberRef.current.value, email: emailRef.current.value, check_in: checkInDateRef.current.value, check_out: checkOutDateRef.current.value, message: messageRef.current.value })
            })

        } catch (error) {
            console.error(error);
        }
    };

    const handleSubmit = (event) => {
        event.preventDefault(); // Prevent default form submission behavior
    };


    return (
        <div>
            <Header />
           
                <div id="preloader-active">
                    <div class="preloader d-flex align-items-center justify-content-center">
                        <div class="preloader-inner position-relative">
                            <div class="preloader-circle"></div>
                            <div class="preloader-img pere-text">
                                <strong> REVMAN</strong>
                            </div>
                        </div>
                    </div>
                </div>
            <main>
                {/* <!-- slider Area Start--> */}
                <div className="slider-area" style={{ height: "auto" }}>
                    {/* Mobile Menu */}
                    <div className="slider-active dot-style">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div className="slider-active">
                                        <div
                                            className="single-slider hero-overly slider-height2 d-flex align-items-center"
                                            style={{ backgroundImage: "url('assets/img/0.jpg')" }}
                                        >
                                            <div class="container">
                                                <div class="row ">
                                                    <div class="col-md-11 offset-xl-1 offset-lg-1 offset-md-1">
                                                        <div class="hero-caption my-element">
                                                            <span>Rooms</span>
                                                            <h2>{item.room_title}</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="make-customer-area customar-padding fix" style={{ marginTop: "100px" }}>
                    <div class="container-fluid p-0">
                        <div class="row">
                            <div class="col-xl-5 col-lg-6">
                                <div class="customer-img mb-130">
                                    <img src={`http://localhost:5000/room/${item.main_image}`} class="customar-img1" alt="main image" />
                                    <img src="assets/img/customer/customar2.png" class="customar-img2" alt="" />
                                    <div class="service-experience heartbeat">
                                        <h3>25 Years of Service<br />Experience</h3>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-xl-5 col-lg-6">
                                <div class="customer-caption">
                                    <span>Our Room</span>
                                    <h2>{item.room_title}</h2>
                                    <div class="caption-details">
                                        <p >{item.description}</p>
                                        <div class="room-details">
                                            <div class="capacity">
                                                <h3 class="capacity-heading">Capacity</h3>
                                                <div className='d-flex'>
                                                    <p>Adult: {item.capacity_adult}</p>
                                                    <p>Child: {item.capacity_child}</p>
                                                </div>

                                            </div>
                                            <div class="price">
                                                <h3 class="price-heading">Price</h3>
                                                <p class="price-value">BTN{item.price}/per night</p>
                                            </div>
                                        </div>

                                        <a data-bs-toggle="modal" onClick={() => settingRoomCode(item.room_code)} data-bs-target="#exampleModal" class="btn more-btn1" style={{color:"white"}}>Book Now <i class="ti-angle-right"></i> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                {/* booking form */}

                <div class="modal fade popup_form" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <form className="booking-form" onSubmit={handleSubmit}>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">BOOKING FORM</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <label>
                                        Name:
                                        <input placeholder="ENTER YOUR NAME" type="text" ref={nameRef} required />
                                    </label>
                                    <label>
                                        Phone Number:
                                        <input placeholder="ENTER YOUR PHONE NUMBER" type="number" ref={numberRef} required />
                                    </label>
                                    <label>
                                        Email:
                                        <input placeholder="EMTER EMAIL" type="email" ref={emailRef} required />
                                    </label>
                                    <label>
                                        Check-in date:
                                        <input type="datetime-local" ref={checkInDateRef} required />
                                    </label>
                                    <label>
                                        Check-out date:
                                        <input type="datetime-local" ref={checkOutDateRef} required />
                                    </label>
                                    <label>
                                        Message:
                                        <textarea placeholder="LEAVE MESSAGE FOR US" type="text" ref={messageRef} required />
                                    </label>

                                </div>
                                <div class="modal-footer">
                                    <a href="#" data-bs-dismiss="modal" class="genric-btn danger radius">Cancel</a>
                                    <button data-bs-dismiss="modal" type="submit" onClick={roomBooking} class="btn btn-primary">Confirm</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <section className="room-area mb-100 section-top-border" style={{}}>
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-xl-8">
                                {/*font-back-tittle  */}
                                <div className="font-back-tittle">
                                    <div className="archivment-front">
                                        <h3 style={{ fontSize: "50px" }}>{item.room_title} Gallery</h3>
                                    </div>
                                    <h3 style={{ fontSize: "80px" }} className="archivment-back">{item.room_title} Gallery</h3>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

                <div className='mb-200'>

                    <SlideshowLightbox theme="lightbox" className='d-flex flex-wrap justify-content-center'>
                        {item.gallery.map((image, i) => (

                            <img
                                src={`http://localhost:5000/room/${image}`}
                                alt={`Image ${i}`}
                                style={{ padding: '10px', width: "auto", height: "300px" }}
                            />
                        ))}
                    </SlideshowLightbox>
                </div>

            </main>



            {/* footer */}

            <Footer />
        </div>
    )
}

