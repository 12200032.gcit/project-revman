import React, { useEffect, useState } from "react";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import OwlCarousel from "react-owl-carousel";
import axios from 'axios';
import { Link } from 'react-router-dom';
import Header from "./Header";
import Footer from "./Footer";

export default function Home() {

  // get room data
  const [data, setData] = useState([]);
  const [foodData, setFoodData] = useState([]);

  useEffect(() => {
    axios.get(`http://localhost:5001/food/getFoods`)
      .then(res => {
        setFoodData(res.data);
      })
      .catch(err => {
        console.log(err);
      });

      axios.get(`http://localhost:5001/room/getRooms`)
      .then(res => {
        setData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);


  const [carouselState, setCarouselState] = useState({
    responsive: {
      500: {
        items: 1,
        nav: true
      },
      900: {
        items: 2,
        nav: false
      },
      1300: {
        items: 3,
        loop: true,
      }
    },
    dot: true,
    loop: true,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
  });



  return (
    <>
      <div style={{ overflow: "hidden" }}>
        <div id="preloader-active">
          <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
              <div class="preloader-circle"></div>
              <div class="preloader-img pere-text">
                <strong> REVMAN</strong>
              </div>
            </div>
          </div>
        </div>

        {/* header start */}
        <Header/>
        {/* header end */}

        <main>
          <div className="slider-area">
            {/* Mobile Menu */}
            <div className="slider-active dot-style">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <div className="slider-active dot-style">
                      <div
                        className="single-slider hero-overly slider-height d-flex align-items-center"
                        style={{ backgroundImage: "url('assets/img/0.jpg')", height: "1000px" }}
                      >
                        <div className="container">
                          <div className="row justify-content-center text-center">
                            <div className="col-xl-9">
                              <div className="h1-slider-caption">
                                <div className="my-element">
                                  <h1 className="my-element__title">Welcome to Our Hotel & resturent</h1>
                                  <h3 className="my-element__subtitle">REVMEN</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <div className="slider-active dot-style">
                      <div
                        className="single-slider hero-overly slider-height d-flex align-items-center"
                        style={{ backgroundImage: "url('assets/img/0.jpg')", height: "1000px" }}
                      >
                        <div className="container">
                          <div className="row justify-content-center text-center">
                            <div className="col-xl-9">
                              <div className="h1-slider-caption">
                                <div className="my-element">
                                  <h1 className="my-element__title">Welcome to Our Hotel & resturent</h1>
                                  <h3 className="my-element__subtitle">REVMEN</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <div className="slider-active dot-style">
                      <div
                        className="single-slider hero-overly slider-height d-flex align-items-center img-fluid"
                        style={{ backgroundImage: "url('assets/img/0.jpg')", height: "1000px" }}
                      >
                        <div className="container">
                          <div className="row justify-content-center text-center">
                            <div className="col-xl-9">
                              <div className="h1-slider-caption">
                                <div className="my-element">
                                  <h1 className="my-element__title">Welcome to Our Hotel & resturent</h1>
                                  <h3 className="my-element__subtitle">REVMEN</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div >
          </div >


          {/* <!-- Booking Room Start--> */}
          <div class="booking-area my-element">
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <form action="">
                    <div class="booking-wrap d-flex justify-content-between">

                      {/* <!-- select in date --> */}
                      <div class="single-select-box mb-30 ">
                        {/* <!-- select out date --> */}
                        <div class="boking-tittle">
                          <span> Check In Date:</span>
                        </div>
                        <div class="boking-datepicker">
                          <input type='date' id="datepicker1" placeholder="10/12/2020" />
                        </div>
                      </div>
                      {/* <!-- Single Select Box --> */}
                      <div class="single-select-box mb-30">
                        {/* <!-- select out date --> */}
                        <div class="boking-tittle">
                          <span>Check OutDate:</span>
                        </div>
                        <div class="boking-datepicker">
                          <input type='date' id="datepicker2" placeholder="12/12/2020" />
                        </div>
                      </div>
                      {/* <!-- Single Select Box --> */}
                      <div class="single-select-box mb-30">
                        <div class="boking-tittle">
                          <span>Adults:</span>
                        </div>
                        <div class="select-this">
                          <form action="#">
                            <div class="select-itms">
                              <select name="select" id="select1">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                              </select>
                            </div>
                          </form>
                        </div>
                      </div>
                      {/* <!-- Single Select Box --> */}
                      <div class="single-select-box mb-30">
                        <div class="boking-tittle">
                          <span>Children:</span>
                        </div>
                        <div class="select-this">
                          <form action="#">
                            <div class="select-itms">
                              <select name="select" id="select2">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                              </select>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div class="single-select-box pt-45 mb-30">
                        <a href="#" class="btn available select-btn">Check Availability</a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Booking Room End--> */}


          {/* <!-- Make customer Start--> */}
          <section class="make-customer-area customar-padding my-element  fix" style={{ marginTop: "150px" }}>
            <div class="container-fluid p-0">
              <div class="row">
                <div class="col-xl-5 col-lg-6">
                  <div class="customer-img mb-130">
                    <img src="assets/img/room3.jpg" class="customar-img1" alt="" />
                    <img src="assets/img/customer/customar2.png" class="customar-img2" alt="" />
                    <div class="service-experience heartbeat">
                      <h3>25 Years of Service<br />Experience</h3>
                    </div>
                  </div>
                </div>
                <div class=" col-xl-5 col-lg-6">
                  <div class="customer-caption">
                    <span>About our </span>
                    <h2>REVMAN Hotel & resturent</h2>
                    <div class="caption-details">
                      <p class="pera-dtails">Nestled amidst the serene beauty of Bhutan, Revman Hotel and Restaurant is a luxurious retreat that redefines hospitality. </p>
                      <p >With its breathtaking views of the surrounding mountains and lush greenery, Revman Hotel and Restaurant is the perfect destination for a romantic getaway, a family vacation, or a corporate retreat. Our commitment to exceptional service and hospitality ensures that every guest feels like royalty.
                      </p>
                      <a href="#" class="btn more-btn1">Learn More <i class="ti-angle-right"></i> </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* <!-- Make customer End--> */}
          {
            /* Room Start */
          }

          <section className="room-area make-customer-area customar-padding fix my-element" style={{ backgroundColor: "#D9D9D982" }}>
            <div class="">
              <div class="row justify-content-center">
                <div class="col-xl-8">
                  {/* <!--font-back-tittle  --> */}
                  <div class="font-back-tittle mb-100">
                    <div class="archivment-front">
                      <h3>Our Rooms</h3>
                    </div>
                    <h3 class="archivment-back">Our Rooms</h3>
                  </div>
                </div>
              </div>


              <OwlCarousel className="owl-theme" {...carouselState}>
                {data.map((item, index) => (
                  <div className="single-room mb-50">
                    <div className="room-img">

                      <img src={`http://localhost:5000/room/${item.main_image}`} className="image-fluid" alt='room_image' />

                    </div>
                    <div className="room-caption">
                      <h3 className='text-center'>
                        <a href="rooms.html">{item.room_title}</a>
                      </h3>
                      <div className="text-container">
                        <p className='text-center text-limit'>{item.description}</p>
                      </div>

                      <div className="per-night">
                        <span>

                          <div className="single-select-box pt-4 mb-30 text-center">
                            <a href="1" className="btn select-btn">View</a>
                          </div>

                        </span>
                      </div>
                    </div>
                  </div>
                ))}
              </OwlCarousel>



              <div className="row justify-content-center text-center">
                <div className="room-btn">
                  <a href="7" className="btn mb-4 view-btn1">
                    View all Room  <i className="ti-angle-right" />{' '}
                  </a>
                </div>
              </div>
            </div>
          </section>;
          {
            /* Room End */
          }

          {/* <!-- Dining Start --> */}
          <div class="dining-area dining-padding-top my-element">
            {/* <!-- Single Left img --> */}
            <div class="single-dining-area left-img">
              <div class="container">
                <div class="row justify-content-end">
                  <div class="col-lg-8 col-md-8">
                    <div class="dining-caption">
                      <span>About our</span>
                      <h3>Hotel Rooms</h3>
                      <p>Revman Hotel, located in the breathtaking beauty of Bhutan, is a luxurious retreat that
                        blends traditional Bhutanese charm with modern comfort. From the moment you arrive, you
                        are greeted with warm hospitality and a sense of tranquility that permeates every aspect of your stay.<br /><br />
                        The hotel's decor is a harmonious blend of Bhutanese aesthetics and contemporary design, creating an ambiance that is both sophisticated and
                        inviting. Our rooms and suites are tastefully appointed with modern amenities and offer stunning views of the
                        surrounding mountains and lush greenery.</p>
                      <a href="#" class="btn border-btn">Learn More <i class="ti-angle-right"></i> </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* <!-- single Right img --> */}

          </div>
          {/* /* <!-- Dining End --> */}

          <section class="make-customer-area customar-padding fix mt-170 pt-100" style={{ backgroundColor: "#D9D9D982" }}>
            <div class="container-fluid p-0">
              <div class="row">

                <div class=" col-xl-5 col-lg-6">
                  <div class="customer-caption">
                    <span>TOP-Rated ROOMs </span>
                    <h2>Red deluxe room</h2>
                    <div class="caption-details mb-50">

                      <p >With its breathtaking views of the surrounding mountains and lush greenery, Revman Hotel and Restaurant is the perfect destination for a romantic getaway, a family vacation, or a corporate retreat. Our commitment to exceptional service and hospitality ensures that every guest feels like royalty.
                      </p>
                      <a href="#" class="btn more-btn1">View more <i class="ti-angle-right"></i> </a>
                    </div>
                  </div>
                </div>
                <div class="col-xl-5 col-lg-6">
                  <div class="customer-img mb-130">
                    <img src="assets/img/room3.jpg" class="customar-img1" alt="" />
                    <img src="assets/img/customer/customar2.png" class="customar-img2" alt="" />
                    <div class="service-experience heartbeat">
                      <h3>25 Years of Service<br />Experience</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section class="make-customer-area customar-padding fix" style={{ marginTop: "100px" }}>
            <div class="container-fluid p-0">
              <div class="row">
                <div class="col-xl-5 col-lg-6">
                  <div class="customer-img mb-130">
                    <img src="assets/img/room3.jpg" class="customar-img1" alt="" />
                    <img src="assets/img/customer/customar2.png" class="customar-img2" alt="" />
                    <div class="service-experience heartbeat">
                      <h3>25 Years of Service<br />Experience</h3>
                    </div>
                  </div>
                </div>
                <div class=" col-xl-5 col-lg-6">
                  <div class="customer-caption">
                    <span>TOP-Rated ROOMs </span>
                    <h2>Green deluxe room</h2>
                    <div class="caption-details">

                      <p >With its breathtaking views of the surrounding mountains and lush greenery, Revman Hotel and Restaurant is the perfect destination for a romantic getaway, a family vacation, or a corporate retreat. Our commitment to exceptional service and hospitality ensures that every guest feels like royalty.
                      </p>
                      <a href="#" class="btn more-btn1">View More <i class="ti-angle-right"></i> </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>



          {/* View foods */}
          <section className="room-area make-customer-area customar-padding fix" style={{ backgroundColor: "#D9D9D982" }}>
            <div class="">
              <div class="row justify-content-center">
                <div class="col-xl-8">
                  {/* <!--font-back-tittle  --> */}
                  <div class="font-back-tittle mb-100">
                    <div class="archivment-front">
                      <h3>Our Foods</h3>
                    </div>
                    <h3 class="archivment-back">Our Foods</h3>
                  </div>
                </div>
              </div>


              <OwlCarousel className="owl-theme" {...carouselState}>
                {foodData.map((item, index) => (
                  <div className="single-room mb-50">
                    <div className="room-img">

                      <img src={`http://localhost:5000/food/${item.main_image}`} className="image-fluid" alt='23' />

                    </div>
                    <div className="room-caption">
                      <h3 className='text-center'>
                        <a href="rooms.html">{item.food_title}</a>
                      </h3>
                      <div className="text-container">
                        <p className='text-center text-limit'>{item.description}</p>
                      </div>

                      <div className="per-night">
                        <span>

                          <div className="single-select-box pt-4 mb-30 text-center">
                            <a href="1" className="btn select-btn">View</a>
                          </div>

                        </span>
                      </div>
                    </div>
                  </div>
                ))}
              </OwlCarousel>



              <div className="row justify-content-center text-center">
                <div className="room-btn">
                  <a href="7" className="btn mb-4 view-btn1">
                    View all Food  <i className="ti-angle-right" />{' '}
                  </a>
                </div>
              </div>
            </div>
          </section>;
          {
            /* Room End */
          }
          {/* <!-- Dining Start --> */}
          <div class="dining-area dining-padding-top">
            {/* <!-- Single Left img --> */}

            {/* <!-- single Right img --> */}
            <div class="single-dining-area right-img">
              <div class="container">
                <div class="row justify-content-start">
                  <div class="col-lg-8 col-md-8">
                    <div class="dining-caption text-right">
                      <span>About our</span>
                      <h3>Hotel Restaurant</h3>
                      <p>Revman Restaurant, located in the stunning beauty of Bhutan, is a culinary haven that offers a delightful
                        blend of traditional Bhutanese cuisine and contemporary flavors. From the moment you enter the restaurant,
                        you are transported to a world of sensory delight.<br /> <br />The restaurant's decor is a beautiful fusion of Bhutanese
                        aesthetics and modern design, creating an ambiance that is both elegant and inviting. The warm hospitality
                        of our staff, coupled with the restaurant's stunning mountain views, makes for a truly unforgettable dining
                        experience.<br /><br />Come and experience the magic of Revman Restaurant, where tradition meets innovation and every dish is a
                        celebration of Bhutanese culture and hospitality.</p>
                      <a href="#" class="btn border-btn">Learn More <i class="ti-angle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* /* <!-- Dining End --> */}
          <section class="make-customer-area customar-padding fix  mt-170 pt-100" style={{ backgroundColor: "#D9D9D982" }}>
            <div class="container-fluid p-0">
              <div class="row mt-100">
                <div class="col-xl-5 col-lg-6">
                  <div class="customer-img mb-130">
                    <img src="assets/img/room3.jpg" class="customar-img1" alt="" />
                    <img src="assets/img/customer/customar2.png" class="customar-img2" alt="" />
                    <div class="service-experience heartbeat">
                      <h3>25 Years of Service<br />Experience</h3>
                    </div>
                  </div>
                </div>
                <div class=" col-xl-5 col-lg-6">
                  <div class="customer-caption">
                    <span>TOP-Rated FOODs </span>
                    <h2>Ema Datshe</h2>
                    <div class="caption-details">

                      <p >With its breathtaking views of the surrounding mountains and lush greenery, Revman Hotel and Restaurant is the perfect destination for a romantic getaway, a family vacation, or a corporate retreat. Our commitment to exceptional service and hospitality ensures that every guest feels like royalty.
                      </p>
                      <a href="#" class="btn more-btn1">View More <i class="ti-angle-right"></i> </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section class="make-customer-area customar-padding fix mt-100" >
            <div class="container-fluid p-0">
              <div class="row">

                <div class=" col-xl-5 col-lg-6">
                  <div class="customer-caption">
                    <span>TOP-Rated FOODs </span>
                    <h2>Kawa Dashi</h2>
                    <div class="caption-details mb-50">

                      <p >With its breathtaking views of the surrounding mountains and lush greenery, Revman Hotel and Restaurant is the perfect destination for a romantic getaway, a family vacation, or a corporate retreat. Our commitment to exceptional service and hospitality ensures that every guest feels like royalty.
                      </p>
                      <a href="#" class="btn more-btn1">View more <i class="ti-angle-right"></i> </a>
                    </div>
                  </div>
                </div>
                <div class="col-xl-5 col-lg-6">
                  <div class="customer-img mb-130">
                    <img src="assets/img/room3.jpg" class="customar-img1" alt="" />
                    <img src="assets/img/customer/customar2.png" class="customar-img2" alt="" />
                    <div class="service-experience heartbeat">
                      <h3>25 Years of Service<br />Experience</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section className="gallery" style={{ marginLeft: "-20px", marginBottom: "-20px", marginRight: "-20px" }}>
            <OwlCarousel className="owl-theme" {...carouselState}>
              {foodData.map((item, index) => (
                <img className="imag-fluid" src={`http://localhost:5000/food/${item.main_image}`} alt="" />
              ))}
              {data.map((item, index) => (
                <img className="imag-fluid" src={`http://localhost:5000/room/${item.main_image}`} alt="" />
              ))}
            </OwlCarousel>

          </section>

        </main >
        
        {/* footer start */}
        <Footer/>
        {/* footer end */}

      </div >
    </>
  )
}
