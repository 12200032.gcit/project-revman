import React from 'react'

function Header() {
    return (

        
        <header>
            
            {/* <!-- Header Start --> */}
            <div className="header-area header-sticky " >
                <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: "white" }}>
                    <div className="container-fluid">

                        <div className="logo">
                            <a href="/"><img src="assets/img/logo/white-logo.jpg" alt="logo" style={{ width: "80px" }} /></a>
                        </div>

                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse col-xl-8 col-lg-12" id="navbarNavDropdown">
                            <ul className="navbar-nav text-lg-center">
                                <li className="nav-item">
                                    <a className="nav-link active nav_home" aria-current="page" href="/" >Home</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link nav_room" href="./room">Room</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link nav_food" href="./food">Food</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link nav_about" href="#">About</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link nav_contact" href="./contact">Contact</a>
                                </li>

                            </ul>
                            <div className="nav-button">
                                <a href="./room" className="btn">Book Rooms </a>
                            </div>
                        </div>

                    </div>
                </nav>
            </div>
            {/* <!-- Header End --> */}
        </header>
    )
}

export default Header
