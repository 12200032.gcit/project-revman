import React, { useEffect, useState, useRef } from "react";
import axios from 'axios';
import Header from './Header';
import Footer from './Footer';
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import OwlCarousel from "react-owl-carousel";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import FoodCart from "./FoodCart";

function Food() {

    const rating = 3.5;

    const [activeCategory, setActiveCategory] = useState("vegetarian");
    const [activeSorting, setActiveSorting] = useState("highest");

    const handleCategoryClick = (category, sorting) => {
        setActiveCategory(category);
        setActiveSorting(sorting);
    };

    const fullStars = Math.floor(rating);
    const halfStars = Math.ceil(rating - fullStars);
    const emptyStars = 6 - fullStars - halfStars;

    // get room detail base on category

    const [data, setData] = useState([]);
    // handle category

    useEffect(() => {
        axios.get(`http://localhost:5001/food/food_detail/${activeCategory}/${activeSorting}`)
            .then(res => {
                setData(res.data);
            })
            .catch(err => {
                console.log(err);
            });
    }, [data], [activeCategory]);


    const [carouselState, setCarouselState] = useState({
        responsive: {
            500: {
                items: 1,
                nav: true
            },
            900: {
                items: 2,
                nav: false
            },
            1300: {
                items: 3,
                loop: true,
            }
        },
        dot: true,
        loop: true,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: false,
    });

    const [foodData, setFoodData] = useState([]);

    useEffect(() => {
        axios.get(`http://localhost:5001/food/getFood`)
            .then(res => {
                setFoodData(res.data);
            })
            .catch(err => {
                console.log(err);
            });
    }, [foodData]);

    // find the quantity of room in each category

    let count_single = 0;
    let count_double = 0;
    let count_triple = 0;
    let count_quad = 0;
    foodData.map((item, index) => {
        if (item.category === "vegetarian") {
            count_single++;
        }

        if (item.category === "non-vegetarian") {
            count_double++;
        }

        if (item.category === "vegan") {
            count_triple++;
        }

        if (item.category === "drink") {
            count_quad++;
        }
    });

    // add to cart

    const [food_id, setFood_id] = useState();
    const [food_tittle, setFood_tittle] = useState();
    const [price, setPrice] = useState();
    const quantityRef = useRef(1);
    const [main_image, setMain_image] = useState();

    const savedCart = JSON.parse(localStorage.getItem('cart')); // "[]"

    const [cartItems, setCartItems] = useState(savedCart);

    function setImageIdTittlePrice(food_id, tittle, price, image) {
        setFood_id(food_id);
        setFood_tittle(tittle);
        setPrice(price);
        setMain_image(image);
    }

    const addToCart = async () => {
        try {
            const item = [food_id, food_tittle, price, main_image, quantityRef.current.value];
            setCartItems((prevItems) => [...prevItems, item]);
            toast.success("Logout successfully");
        }
        catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cartItems));
    }, [cartItems]);

    const handleSubmit = (event) => {
        event.preventDefault(); // Prevent default form submission behavior
    };

    return (
        <>
            <div style={{ overflow: "hidden" }}>
                <div id="preloader-active">
                    <div class="preloader d-flex align-items-center justify-content-center">
                        <div class="preloader-inner position-relative">
                            <div class="preloader-circle"></div>
                            <div class="preloader-img pere-text">
                                <strong> REVMAN</strong>
                            </div>
                        </div>
                    </div>
                </div>

                {/* header start */}
                <Header />
                {/* header end */}
                <main>
                    {/* <!-- slider Area Start--> */}
                    <div className="slider-area" style={{ height: "auto" }}>
                        {/* Mobile Menu */}
                        <div className="slider-active dot-style">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div className="slider-active">
                                            <div
                                                className="single-slider hero-overly slider-height2 d-flex align-items-center"
                                                style={{ backgroundImage: "url('assets/img/hero/roomspage_hero.jpg')" }}
                                            >
                                                <div class="container">
                                                    <div class="row ">
                                                        <div class="col-md-11 offset-xl-1 offset-lg-1 offset-md-1">
                                                            <div class="hero-caption my-element">
                                                                <span>Restaurant</span>
                                                                <h2>Our Restaurant</h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="carousel-caption d-none d-md-block my-element">
                                                <h3 style={{ color: "#fff" }}>Indulge in the flavors of Bhutan while being treated to exceptional service in the heart of the majestic Himalayas. </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div className="slider-active ">
                                            <div
                                                className="single-slider hero-overly slider-height2 d-flex align-items-center"
                                                style={{ backgroundImage: "url('assets/img/0.jpg')" }}
                                            >
                                                <div class="container">
                                                    <div class="row ">
                                                        <div class="col-md-11 offset-xl-1 offset-lg-1 offset-md-1">
                                                            <div class="hero-caption my-element">
                                                                <span>Restaurant</span>
                                                                <h2>Our Restaurant</h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="carousel-caption d-none d-md-block my-element">
                                                    <h3 style={{ color: "#fff" }}> A dining experience where RevMan ensures a seamless and unforgettable culinary adventure.
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div className="slider-active">
                                            <div
                                                className="single-slider hero-overly slider-height2 d-flex align-items-center"
                                                style={{ backgroundImage: "url('assets/img/0.jpg')" }}
                                            >
                                                <div class="container">
                                                    <div class="row ">
                                                        <div class="col-md-11 offset-xl-1 offset-lg-1 offset-md-1">
                                                            <div class="hero-caption my-element">
                                                                <span>Restaurant</span>
                                                                <h2>Our Restaurant</h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="carousel-caption d-none d-md-block my-element">
                                            <h3 style={{ color: "#fff" }}>Bhutan's pinnacle of hospitality, offering impeccable reservation services and meticulous management. </h3>
                                        </div>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div >
                    </div >

                    {/* <!-- slider Area End--> */}


                    <section className="room-area r-padding1">
                        <div className="container">
                            <div className="row justify-content-center">
                                <div className="col-xl-8">
                                    {/*font-back-tittle  */}
                                    <div className="font-back-tittle">
                                        <div className="archivment-front">
                                            <h3>Our Food </h3>
                                        </div>
                                        <h3 className="archivment-back">Our Food</h3>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </section>

                    <section class="section-padding">
                        <div class="">
                            <div class="row">
                                <div class="col-xl-9 col-lg-12 mb-5 col-md-0 custom-responsive" >
                                    <div class="blog_left_sidebar">
                                        {/* <!-- Room Start --> */}
                                        <section class="room-area">
                                            <div class="row">
                                                {/* room category start*/}
                                                {data.map((item, index) => (
                                                    <div class="col-xl-4 col-lg-6 col-md-6 p-0">
                                                        <div className="room-design">
                                                            <div className="room-img">
                                                                <a href="#"><img src={`http://localhost:5000/room/${item.main_image}`} className="image-fluid" alt='room_image' style={{ borderTopLeftRadius: "5px", borderTopRightRadius: "5px" }} /></a>
                                                            </div>
                                                            <div className="room-caption">
                                                                <h3 className='text-center'>
                                                                    <a href="#">{item.food_title}</a>
                                                                </h3>
                                                                <div className="star-rating pt-3">
                                                                    {[...Array(fullStars)].map((_, i) => (
                                                                        <i key={i} className="fas fa-star"></i>
                                                                    ))}
                                                                    {[...Array(halfStars)].map((_, i) => (
                                                                        <i key={i} className="fas fa-star-half-alt"></i>
                                                                    ))}
                                                                    {[...Array(emptyStars)].map((_, i) => (
                                                                        <i key={i} className="far fa-star"></i>
                                                                    ))}
                                                                </div>
                                                                <div class="per-night pb-4 pt-2 d-flex ">
                                                                    <span><u>BTN</u>{item.price} <span>/-</span></span>
                                                                </div>
                                                                <div className='text-center pt-2'>
                                                                    <a><Link to="/view_food" state={item} class="genric-btn warning-border radius">View</Link></a>
                                                                    <a class="genric-btn warning radius" onClick={() => setImageIdTittlePrice(item.id, item.food_title, item.price, item.main_image)} data-bs-toggle="modal" data-bs-target="#exampleModal">Add</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))}
                                                {/* room category end */}

                                            </div>
                                        </section>
                                        {/* <!-- Room End --> */}

                                        <nav class="blog-pagination justify-content-center d-flex">
                                            <ul class="pagination">
                                                <li class="page-item">
                                                    <a href="#" class="page-link" aria-label="Previous">
                                                        <i class="ti-angle-left"></i>
                                                    </a>
                                                </li>
                                                <li class="page-item ">
                                                    <a href="#" class="page-link">1</a>
                                                </li>
                                                <li class="page-item active">
                                                    <a href="#" class="page-link">2</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#" class="page-link" aria-label="Next">
                                                        <i class="ti-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>

                                {/* side bar start*/}
                                <div className="col-xl-3 col-lg-6 col-md-6">
                                    <div className="blog_right_sidebar">
                                        <aside className="single_sidebar_widget search_widget">
                                            <form action="#">
                                                <div className="form-group">
                                                    <div className="input-group mb-4">
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            style={{ fontSize: "20px" }}
                                                            placeholder="Search Rooms"

                                                        />
                                                        <div className="input-group-append">
                                                            <button className="btn" type="button" style={{ fontSize: "20px" }}>
                                                                <i className="ti-search"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button className="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" style={{ fontSize: "20px", height: "70px" }} type="submit">
                                                    Search
                                                </button>
                                            </form>
                                        </aside>

                                        <aside className="single_sidebar_widget post_category_widget">
                                            <h4 className="widget_title">Category</h4>
                                            <ul className="list cat-list">
                                                <li>
                                                    <a style={{ cursor: "pointer" }} className={`d-flex ${activeCategory === "vegetarian" ? "active" : ""}`} onClick={() => handleCategoryClick("vegetarian", activeSorting)}>
                                                        <p>Vegetarian</p>
                                                        <p>({count_single})</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a style={{ cursor: "pointer" }} className={`d-flex ${activeCategory === "non-vegetarian" ? "active" : ""}`} onClick={() => handleCategoryClick("non-vegetarian", activeSorting)}>
                                                        <p>Non-Vegetarian</p>
                                                        <p>({count_double})</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a style={{ cursor: "pointer" }} className={`d-flex ${activeCategory === "vegan" ? "active" : ""}`} onClick={() => handleCategoryClick("vegan", activeSorting)}>
                                                        <p>Vegan</p>
                                                        <p>({count_triple})</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a style={{ cursor: "pointer" }} className={`d-flex ${activeCategory === "drink" ? "active" : ""}`} onClick={() => handleCategoryClick("drink", activeSorting)}>
                                                        <p>Drinks</p>
                                                        <p>({count_quad})</p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </aside>

                                        <aside className="single_sidebar_widget post_category_widget">
                                            <h4 className="widget_title">Sort by price</h4>
                                            <ul className="list cat-list" style={{ cursor: "pointer" }}>
                                                <li>
                                                    <a style={{ cursor: "pointer" }} className={`d-flex ${activeSorting === "highest" ? "active" : ""}`} onClick={() => handleCategoryClick(activeCategory, "highest")}>
                                                        <p>Highest Price</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a style={{ cursor: "pointer" }} className={`d-flex ${activeSorting === "lowest" ? "active" : ""}`} onClick={() => handleCategoryClick(activeCategory, "lowest")}>
                                                        <p>Lowest Price</p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </aside>

                                    </div>
                                </div>
                            </div>

                            {/* booking form */}

                            <div class="modal fade popup_form" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form className="booking-form mt-200" onSubmit={handleSubmit}>
                                        <div class="modal-content">
                                            <div class="modal-header">

                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <label>
                                                    ENTER THE NUMBER OF QUANTITY YOU WANT TO HAVE
                                                    <input defaultValue="1" type="number" className="mt-3" ref={quantityRef} required />
                                                </label>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="#" data-bs-dismiss="modal" class="genric-btn danger radius">Cancel</a>

                                                <button type="submit" data-bs-dismiss="modal" class="btn btn-primary" onClick={addToCart}>Confirm</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>


                    </section >
                    <section className="gallery" style={{ marginLeft: "-20px", marginBottom: "-20px", marginRight: "-20px" }}>
                        <OwlCarousel className="owl-theme" {...carouselState}>

                            {data.map((item, index) => (
                                <img className="imag-fluid" src={`http://localhost:5000/room/${item.main_image}`} alt="" />
                            ))}
                        </OwlCarousel>

                    </section>
                </main >

                <div className="food_cart ">
                    <FoodCart />
                </div>

                {/* footer start */}
                < Footer />
                {/* footer end */}
            </div >
            {/* <!-- Modal --> */}

        </>
    )
}


export default Food