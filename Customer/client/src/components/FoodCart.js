import React, { useState, useEffect } from 'react';

const FoodCart = () => {

    // Load cart items from local storage
    const savedCart = localStorage.getItem('cart');

    const [cartItems, setCartItems] = useState([]);

    const deleteFromCart = (itemId) => {
        setCartItems((prevItems) => prevItems.filter((item) => item.id !== itemId));
    };

    let itemCount = cartItems;

    return (
        <>
            <div className="shopping-cart-icon" data-bs-toggle="modal" data-bs-target="#exampleModal2">
                <i class="bi bi-cart3"></i>
                {itemCount > 0 && <div className="notification-badge">{itemCount}</div>}
            </div>

            <div class="modal fade popup_form" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <form className="food-cart mt-20">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div className='cart-container  mt-2'>
                                    <img className='image-fluid ' src='https://th.bing.com/th/id/R.72a1c9e0173b5046c8e647518c0d9608?rik=N7bTlgfV0vu1BQ&riu=http%3a%2f%2fmaggiejs.ca%2fwp-content%2fuploads%2f2015%2f12%2fFast-Food-Meal-Key-%C2%A9-blog-zinmobi-com.jpg&ehk=BHn4UUTcO3MEzzmBxReg47RrqQtNxgroRZ3KybpFJo8%3d&risl=&pid=ImgRaw&r=0'></img>
                                    <div>
                                        <h6>ITEM NAME: <span>shakam dashe</span></h6>
                                        <h6>QUANTITY: <span>2</span></h6>
                                        <h6>PRICE: <span style={{ color: "#FF0000" }}>10000 BTN</span></h6>
                                    </div>
                                    <div className='cart-icon'>
                                        <div>
                                            <i class="bi bi-dash-circle" style={{ color: "#FF0000" }}></i>
                                            <i class="bi bi-plus-circle" style={{ color: "#00FF00" }}></i>
                                        </div>
                                        <i class="bi bi-trash3-fill" style={{ color: "#B22222", marginLeft: "55px" }}></i>
                                    </div>
                                </div>
                                <div className='cart-container mt-2'>
                                    <img className='image-fluid ' src='https://th.bing.com/th/id/R.72a1c9e0173b5046c8e647518c0d9608?rik=N7bTlgfV0vu1BQ&riu=http%3a%2f%2fmaggiejs.ca%2fwp-content%2fuploads%2f2015%2f12%2fFast-Food-Meal-Key-%C2%A9-blog-zinmobi-com.jpg&ehk=BHn4UUTcO3MEzzmBxReg47RrqQtNxgroRZ3KybpFJo8%3d&risl=&pid=ImgRaw&r=0'></img>
                                    <div>
                                        <h6>ITEM NAME: <span>shakam dashe</span></h6>
                                        <h6>QUANTITY: <span>2</span></h6>
                                        <h6>PRICE: <span style={{ color: "#FF0000" }}>10000 BTN</span></h6>
                                    </div>
                                    <div className='cart-icon'>
                                        <div>
                                            <i class="bi bi-dash-circle" style={{ color: "#FF0000" }}></i>
                                            <i class="bi bi-plus-circle" style={{ color: "#00FF00" }}></i>
                                        </div>
                                        <i class="bi bi-trash3-fill" style={{ color: "#B22222", marginLeft: "55px" }}></i>
                                    </div>
                                </div>
                                <div className='cart-container  mt-2'>
                                    <img className='image-fluid ' src='https://th.bing.com/th/id/R.72a1c9e0173b5046c8e647518c0d9608?rik=N7bTlgfV0vu1BQ&riu=http%3a%2f%2fmaggiejs.ca%2fwp-content%2fuploads%2f2015%2f12%2fFast-Food-Meal-Key-%C2%A9-blog-zinmobi-com.jpg&ehk=BHn4UUTcO3MEzzmBxReg47RrqQtNxgroRZ3KybpFJo8%3d&risl=&pid=ImgRaw&r=0'></img>
                                    <div className='item-detail'>
                                        <h6>ITEM NAME: <span>shakam dash</span></h6>
                                        <h6>QUANTITY: <span>2</span></h6>
                                        <h6>PRICE: <span style={{ color: "#FF0000" }}>10000 BTN</span></h6>
                                    </div>
                                    <div className='cart-icon'>
                                        <div>
                                            <i class="bi bi-dash-circle" style={{ color: "#FF0000" }}></i>
                                            <i class="bi bi-plus-circle" style={{ color: "#00FF00" }}></i>
                                        </div>
                                        <i class="bi bi-trash3-fill" style={{ color: "#B22222", marginLeft: "38px" }}></i>
                                    </div>
                                </div>

                                <h4 className='total-amount'>TOTAL AMOUNT : <span style={{ color: "#FF0000" }}>12000 BTN</span></h4>
                            </div>
                            <div class="modal-footer">
                                <a href="#" data-bs-dismiss="modal" class="genric-btn danger radius">Cancel</a>
                                <button data-bs-dismiss="modal" type="submit" class="btn btn-primary">Order Now</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
};

export default FoodCart;
