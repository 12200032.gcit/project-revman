import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate
} from "react-router-dom";
import Room from "./routes/room/Room";

//components

import Login from "./components/Login";
import Dashboard from "./components/Dashboard";
import Food from "./routes/food/Food";
import AddRoom from "./routes/room/AddRoom";
import UpdateRoom from "./routes/room/UpdateRoom";
import EditRoom from "./routes/room/EditRoom";
import ReservedRoom from "./routes/room/ReservedRoom";
import CustomerDetail from "./routes/customer/CustomerDetail";
import AddFood from "./routes/food/AddFood";
import UpdateFood from "./routes/food/UpdateFood";
import EditFood from "./routes/food/EditFood";
import RoomOrder from "./routes/food/order_management/RoomOrder";
import TableOrder from "./routes/food/order_management/TableOrder";

toast.configure();

function App() {

  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const setAuth = boolean => {
    setIsAuthenticated(boolean);
  };

  const checkAuthenticated = async () => {
    try {
      const res = await fetch("http://localhost:5000/authentication/verify", {
        method: "GET",
        headers: { revman_token: localStorage.token }

      });

      const parseRes = await res.json();
      parseRes === true ? setIsAuthenticated(true) : setIsAuthenticated(false);

    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    checkAuthenticated();
  }, []);






  return (
    <Router>
      <div className="container">
        <Routes>
          <Route
            path="/"
            element={
              !isAuthenticated ? (
                <Login setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/dashboard" />
              )
            }
          />
          <Route
            path="/dashboard"
            element={
              isAuthenticated ? (
                <Dashboard setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />
          <Route
            path="/room"
            element={
              isAuthenticated ? (
                <Room setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />
          <Route
            path="/food"
            element={
              isAuthenticated ? (
                <Food setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />
          <Route
            path="room/add"
            element={
              isAuthenticated ? (
                <AddRoom setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />
          <Route
            path="room/update"
            element={
              isAuthenticated ? (
                <UpdateRoom setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />
          <Route
            path="room/update/edit"
            element={
              isAuthenticated ? (
                <EditRoom setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />

          <Route
            path="room/reservation"
            element={
              isAuthenticated ? (
                <ReservedRoom setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />
          <Route
            path="/customer"
            element={
              isAuthenticated ? (
                <CustomerDetail setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />
          <Route
            path="food/add"
            element={
              isAuthenticated ? (
                <AddFood setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />

          <Route
            path="food/update"
            element={
              isAuthenticated ? (
                <UpdateFood setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />
          <Route
            path="food/update/edit"
            element={
              isAuthenticated ? (
                <EditFood setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />
          <Route
            path="food/room_service_order"
            element={
              isAuthenticated ? (
                <RoomOrder setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />
           <Route
            path="food/table_order"
            element={
              isAuthenticated ? (
                <TableOrder setAuth={setIsAuthenticated} />
              ) : (
                <Navigate to="/" />
              )
            }
          />

        </Routes>

      </div>
    </Router>
  );
};

export default App;