import React, { useState } from "react";
import { toast } from "react-toastify";
import 'bootstrap/dist/css/bootstrap.min.css';
import "../css/login.css";

const Login = ({ setAuth }) => {

    const [inputs, setInputs] = useState({
        email: "",
        password: ""
    });

    const { email, password } = inputs;

    const onChange = e =>
        setInputs({ ...inputs, [e.target.name]: e.target.value });

    const onSubmitForm = async e => {
        e.preventDefault();
        try {
            const body = { email, password };
            const response = await fetch(
                "http://localhost:5000/authentication/login",
                {
                    method: "POST",
                    headers: {
                        "Content-type": "application/json"
                    },
                    body: JSON.stringify(body)
                }
            );

            const parseRes = await response.json();


            if (parseRes.revmanToken) {
                localStorage.setItem("token", parseRes.revmanToken);
                setAuth(true);
                toast.success("Logged in Successfully");
            } else {
                setAuth(false);
                toast.error(parseRes);
            }
        } catch (err) {
            console.error(err.message);
        }
    };

    return (
        <div className="d-flex  login_content">
            <div>
                <img className="admin_img" src="https://img.freepik.com/free-vector/seo-search-engine-optimization-concept_107791-9635.jpg?w=1060&t=st=1683526175~exp=1683526775~hmac=c2d4553bd99c87ed6b797ef83aa90adf31508da6e26648914032475d7d5a3e8d" alt="img"></img>
            </div>
            <div className="login_form">
                <h1 className="text-center text">ADMIN LOGIN</h1>
                <br/>
                <form onSubmit={onSubmitForm}>
                    <input
                        type="text"
                        name="email"
                        value={email}
                        placeholder="email"
                        onChange={e => onChange(e)}
                    />
                    
                    <input
                        type="password"
                        name="password"
                        value={password}
                        placeholder="password"
                        onChange={e => onChange(e)}
                    />
            
                    <input type="submit" className="submit" value={"Submit"}></input>
                    
                    <p className="text-center forgot">forgot <span> password?</span></p>
                </form>

            </div>


        </div>

    );
};

export default Login;