import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { Link } from 'react-router-dom';
import "../../css/sidebar.css";
import "../../css/room.css";
import logo from "../../assets/logo.png";
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';


const ReservedRoom = ({ setAuth }) => {
  const [name, setName] = useState("");

  const getProfile = async () => {
    try {
      const res = await fetch("http://localhost:5000/dashboard/", {
        method: "POST",
        headers: { revman_token: localStorage.token }
      });

      const parseData = await res.json();
      setName(parseData.user_name);
    } catch (err) {
      console.error(err.message);
    }
  };

  const logout = async e => {
    e.preventDefault();
    try {
      localStorage.removeItem("token");
      setAuth(false);
      toast.success("Logout successfully");
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getProfile();
  }, []);

  // Date and Time
  setInterval(updateTime, 1000);
  const now = new Date().toLocaleTimeString();
  const nowDate = new Date().toDateString();

  const [time, setTime] = useState(now);
  const [date, setDate] = useState(nowDate);

  function updateTime() {
    const newTime = new Date().toLocaleTimeString();
    const newDate = new Date().toDateString();
    setDate(newDate);
    setTime(newTime);
  }

  // get data for panding
  const [data, setData] = useState([]);

  useEffect(() => {
    axios.get(`http://localhost:5000/room/pandingDetail`)
      .then(res => {
        setData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [data]);

  // get data for avaibility
  const [roomdata, setRoomData] = useState([]);

  useEffect(() => {
    axios.get(`http://localhost:5000/room/roomavaibility`)
      .then(res => {
        setRoomData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [roomdata]);

  // get data for check-in

  const [checkindata, setCheckInData] = useState([]);

  useEffect(() => {
    axios.get(`http://localhost:5000/room/check_in_customer`)
      .then(res => {
        setCheckInData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [checkindata]);

  // setting id 

  const [id, setId] = useState();
  const [active, setActive] = useState();
  function setNewId(id, bar) {
    setId(id);
    setActive(bar);
  }

  // reject Request


  const rejectRequest = async (id) => {
    try {
      confirmAlert({
        title: 'Confirm',
        message: 'Are you sure you want to reject the booking request?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              axios.delete(`http://localhost:5000/room/rejectRequest/${id}`);
              toast.success('Rejected successfully');
            },
          },
          {
            label: 'No',
            onClick: () => { },
          },
        ],
      });

    } catch (err) {
      console.error(err.message);
    }
  };

  // confirmRequest
  const confirmtRequest = async (id) => {
    try {
      confirmAlert({
        title: 'Confirm',
        message: 'Are you sure you want to confirm the booking request?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              axios.post(`http://localhost:5000/room/confirmRequest/${id}`);
              toast.success('Confirm successfully');
            },
          },
          {
            label: 'No',
            onClick: () => { },
          },
        ],
      });

    } catch (err) {
      console.error(err.message);
    }
  };

  // check out request

  const checkOutRequest = async (id) => {
    try {
      confirmAlert({
        title: 'Confirm',
        message: 'Are you sure you want to Check out this person?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              axios.post(`http://localhost:5000/room/checkOutRequest/${id}`);
              toast.success('Check Out successfully');
            },
          },
          {
            label: 'No',
            onClick: () => { },
          },
        ],
      });

    } catch (err) {
      console.error(err.message);
    }
  };


  return (
    <div>
      <div class="l-navbar" id="bx-x">
        <nav class="sidebar">
          <img className="logo" src={logo} alt="logo" width={"110px"} height={"110px"} />
          <div class="menu-bar">
            <div class="menu">
              <hr></hr>
              <ul class="menu-links">
                <li class="nav-link">
                  <Link to="/dashboard">
                    <i class='bx bx-home-alt icon' ></i>
                    <span class="text nav-text">Dashboard</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link className="dashboard" to="/room">
                    <i class='bx bx-bar-chart-alt-2 icon' ></i>
                    <span class="text nav-text">Room</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/food">
                    <i class='bx bx-bell icon'></i>
                    <span class="text nav-text">Food</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/food">
                    <i class='bx bx-pie-chart-alt icon' ></i>
                    <span class="text nav-text">Customer</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/contant">
                    <i class='bx bx-heart icon' ></i>
                    <span class="text nav-text">Contact</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/money">
                    <i class='bx bx-wallet icon' ></i>
                    <span class="text nav-text">Money</span>
                  </Link>
                </li>

              </ul>
            </div>

            <div class="bottom-content">

              <li class="logout">
                <a onClick={e => logout(e)}>
                  <i class='bx bx-log-out icon' ></i>
                  <span class="text nav-text">Logout</span>
                </a>
              </li>

            </div>
          </div>

        </nav>

      </div>
      <section class="home">
        <div class="top-bar sticky-top">
          <div class=" align-items-center">
            <div class="col-md-6">
              <div class="form">
                <i class='bx bx-search icon'></i>
                <input type="text" class="form-control form-input" placeholder="Search for people" />
              </div>
            </div>
          </div>
          <h4 class="text">Welcome {name}</h4>
          <h4 class="text">{date}</h4>
          <h4 class="text">{time}</h4>
        </div>

        <div className="content">
          <Link to={"/room"} className="position-fixed">
            <FontAwesomeIcon icon={faCircleArrowLeft} bounce size="xl" style={{ color: "#445c4c", zIndex: "100" }} />
          </Link>
          <div>
            <ul class="nav nav-tabs" id="myTab" role="tablist" defaultActiveKey="panding">
              <li class="nav-item" role="presentation">
                <button class="nav-link active" eventKey="panding" data-bs-toggle="tab" data-bs-target="#panding" type="button" role="tab" aria-controls="panding" aria-selected="true">panding</button>
              </li>
              <li class="nav-item" role="presentation">
                <button class="nav-link" eventKey="check-in" data-bs-toggle="tab" data-bs-target="#check-in" type="button" role="tab" aria-controls="check-in" aria-selected="false">check-in</button>
              </li>
              <li class="nav-item" role="presentation">
                <button class="nav-link" eventKey="triple" data-bs-toggle="tab" data-bs-target="#triple" type="button" role="tab" aria-controls="triple" aria-selected="false">Room Availability</button>
              </li>
              <li class="nav-item" role="presentation">
                <button class="nav-link" eventKey="quad" data-bs-toggle="tab" data-bs-target="#quad" type="button" role="tab" aria-controls="quad" aria-selected="false">Quad Room</button>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane  show active" id="panding" role="tabpanel" aria-labelledby="panding-tab">
                {/* Table */}
                <table>
                  <tr className="tableHead">
                    <th >SL No</th>
                    <th >Name</th>
                    <th >Phone</th>
                    <th>Email</th>
                    <th>Check In</th>
                    <th>Check Out</th>
                    <th>Room Code</th>
                    <th>Message</th>

                  </tr>
                  <hr style={{ width: "1000%", }} />


                  {data.map((item, index) => (
                    <tr key={item.id}>
                      <td>{index + 1}</td>

                      <td className="text-truncate">{item.name}</td>
                      <td>{item.phone_number}</td>
                      <td className="text-truncate">{item.email}</td>
                      <td>Date:{new Date(item.check_in).toLocaleDateString()} <br />
                        Time: {new Date(item.check_in).toLocaleTimeString()}</td>
                      <td>Date:{new Date(item.check_out).toLocaleDateString()} <br />
                        Time: {new Date(item.check_out).toLocaleTimeString()}</td>
                      <td>{item.room_code}</td>
                      <td><div className="text-truncate">{item.message}</div></td>
                      <td><i onClick={() => { confirmtRequest(item.id) }} class='bx bxs-check-circle'></i></td>
                      <td><i onClick={() => { rejectRequest(item.id) }} class='bx bxs-x-circle'></i></td>
                      <td><i onClick={() => { setNewId(item.id, "panding") }} data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="bi bi-info-circle-fill info"></i></td>

                    </tr>
                  ))}

                </table>
              </div>

              {/* Check in part */}
              <div class="tab-pane " id="check-in" role="tabpanel" aria-labelledby="check-in-tab">
                {/* table */}
                <table>
                  <tr className="tableHead">
                    <th >SL No</th>
                    <th >Name</th>
                    <th >Phone</th>
                    <th>Email</th>
                    <th>Check In</th>
                    <th>Check Out</th>
                    <th>Room Code</th>
                    <th>Message</th>

                  </tr>
                  <hr style={{ width: "1000%", }} />


                  {checkindata.map((item, index) => (
                    <tr key={item.id}>
                      <td>{index + 1}</td>

                      <td className="text-truncate">{item.name}</td>
                      <td>{item.phone_number}</td>
                      <td className="text-truncate">{item.email}</td>
                      <td>Date:{new Date(item.check_in).toLocaleDateString()} <br />
                        Time: {new Date(item.check_in).toLocaleTimeString()}</td>
                      <td>Date:{new Date(item.check_out).toLocaleDateString()} <br />
                        Time: {new Date(item.check_out).toLocaleTimeString()}</td>
                      <td>{item.room_code}</td>
                      <td><div className="text-truncate">{item.message}</div></td>
                      <td><i onClick={() => { checkOutRequest(item.id) }} class='bx bxs-check-circle'></i></td>
                      <td><i onClick={() => { setNewId(item.id, "check-in") }} data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="bi bi-info-circle-fill info"></i></td>

                    </tr>
                  ))}

                </table>
              </div>

              {/* avaibility */}
              <div class="tab-pane " id="triple" role="tabpanel" aria-labelledby="triple-tab">
                <table>
                  <tr className="tableHead">
                    <th>SL No</th>
                    <th>Room Name</th>
                    <th>Room Code</th>
                    <th>Price</th>
                    <th>Capacity</th>
                    <th>Description</th>

                  </tr>
                  <hr style={{ width: "900%", }} />


                  {roomdata.map((item, index) => (
                    <tr key={item.id}>
                      <td>{index + 1}</td>

                      <td className="text-truncate">{item.room_title}</td>
                      <td>{item.room_code}</td>
                      <td>{item.price}</td>
                      <td>Adult:{item.capacity_adult} <br />
                        Child: {item.capacity_child}</td>
                      <td><div className="text-truncate">{item.description}</div></td>
                      <td><i onClick={() => { setNewId(item.id, "available") }} data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="bi bi-info-circle-fill info"></i></td>

                    </tr>
                  ))}

                </table>


              </div>


              <div class="tab-pane " id="quad" role="tabpanel" aria-labelledby="quad-tab">
                <div>

                </div>

              </div>
            </div>

          </div>
        </div>


        {/* detail information*/}
        <div class="modal " id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content" style={{ width: "150%", marginLeft: "-70px" }}>
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-dialog modal-dialog-scrollable">
                {active === "panding" &&
                  data.map((item, index) => {
                    if (item.id === id) {
                      return (
                        <div>
                          <div>Name: {item.name}</div>
                          <div>Phone Number:{item.phone_number}</div>
                          <div>Email: {item.email}</div>
                          <div>Check-in Date: {new Date(item.check_in).toDateString()}</div>
                          <div>Check-in Time: {new Date(item.check_in).toLocaleTimeString()}</div>
                          <div>Check-out Date: {new Date(item.check_out).toDateString()}</div>
                          <div>Check-out Time: {new Date(item.check_out).toLocaleTimeString()}</div>
                          <div>Booking Time: {new Date(item.booking_time).toLocaleTimeString()}</div>
                          <div>Room code : {item.room_code}</div>
                          <div>Message: {item.message}</div>
                        </div>
                      );
                    }
                    return null;
                  })
                }
                {active === "check-in" &&
                  checkindata.map((item, index) => {
                    if (item.id === id) {
                      return (
                        <div>
                          <div>Name: {item.name}</div>
                          <div>Phone Number:{item.phone_number}</div>
                          <div>Email: {item.email}</div>
                          <div>Check-in Date: {new Date(item.check_in).toDateString()}</div>
                          <div>Check-in Time: {new Date(item.check_in).toLocaleTimeString()}</div>
                          <div>Check-out Date: {new Date(item.check_out).toDateString()}</div>
                          <div>Check-out Time: {new Date(item.check_out).toLocaleTimeString()}</div>
                          <div>Room code : {item.room_code}</div>
                          <div>Message: {item.message}</div>
                        </div>
                      );
                    }
                    return null;
                  })
                }
                {active === "available" &&
                  roomdata.map((item, index) => {
                    if (item.id === id) {
                      return <div>
                        <div>Room Title: {item.room_title}</div>
                        <div>Room Code:{item.room_code}</div>
                        <div>Price: {item.price}</div>
                        <div>Capacity</div>
                        <div>Capacity Adult: {item.capacity_adult}</div>
                        <div>Capacity Child: {item.capacity_child}</div>

                        <div>Room Description: {item.description}</div>
                      </div>

                    }
                    return null;

                  })
                }
              </div>

              <div class="modal-footer">
                {active === "panding" && (
                  <>
                    <button onClick={() => confirmtRequest(id)} data-bs-dismiss="modal" type="button" class="btn btn-primary">
                      Confirm
                    </button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                      Cancel
                    </button>
                  </>
                )}
                {active === "check-in" && (
                  <>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                      close
                    </button>
                  </>
                )}
                {active === "available" && (
                  <>
                   <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                      close
                    </button>
                  </>
                )}

              </div>

            </div>
          </div>
        </div>
      </section >
    </div >
  );

};

export default ReservedRoom;
