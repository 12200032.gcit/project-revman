import React, { useEffect, useState, useRef } from "react";
import { toast } from "react-toastify";
import { Link } from 'react-router-dom';
import { useDropzone } from 'react-dropzone';
import ModalImage from 'react-modal-image';
import axios from 'axios'; // import axios
import "../../css/sidebar.css";
import "../../css/room.css";
import logo from "../../assets/logo.png";
import photo from "../../assets/photo.png";
import gallery_icon from "../../assets/gallery.png";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { useLocation } from "react-router-dom";

const EditFood = ({ setAuth }) => {

    const [name, setName] = useState("");

    const location = useLocation();
    const item = location.state;

    const getProfile = async () => {
        try {
            const res = await fetch("http://localhost:5000/dashboard/", {
                method: "POST",
                headers: { revman_token: localStorage.token }
            });
            const parseData = await res.json();
            setName(parseData.user_name);
        } catch (err) {
            console.error(err.message);
        }
    };

    const logout = async e => {
        e.preventDefault();
        try {
            localStorage.removeItem("token");
            setAuth(false);
            toast.success("Logout successfully");
        } catch (err) {
            console.error(err.message);
        }
    };

    useEffect(() => {
        getProfile();
    }, []);

    // Date and Time
    setInterval(updateTime, 1000);
    const now = new Date().toLocaleTimeString();
    const nowDate = new Date().toDateString();

    const [time, setTime] = useState(now);
    const [date, setDate] = useState(nowDate);

    function updateTime() {
        const newTime = new Date().toLocaleTimeString();
        const newDate = new Date().toDateString();
        setDate(newDate);
        setTime(newTime);
    }

    ////upload image
    ////upload image
    const food_titleRef = useRef(item.food_title);
    console.log(item.food_title);
    const priceRef = useRef(item.price);
    const ingredientRef = useRef(item.ingredient);
    const processofmakingRef = useRef(item.processofmaking);
    const descriptionRef = useRef(item.description); // add a ref to the textarea
    const [main_image, setMain_image] = useState([]);
    const [gallery, setGallery] = useState([]);
    const [image, setImage] = useState(photo);
    const [uploadStatus, setUploadStatus] = useState('');

    const [imageStatus, setImageStatus] = useState(0);

    const handleDrop1 = acceptedFiles => {


        const newImages = [...main_image, ...acceptedFiles];

        // Limit the number of images to 20
        if (acceptedFiles.length + main_image.length > 1) {
            alert('You can only upload a maximum of 1 image');
            return;
        }
        setMain_image(newImages.slice(0));
        setImageStatus(1);
        setImage(main_image);

    };

    const handleDrop2 = acceptedFiles => {
        const newImages = [...gallery, ...acceptedFiles];
        // Limit the number of images to 20
        if (acceptedFiles.length + newGallery.length + gallery.length > 12) {
            alert('You can only upload a maximum of 12 images');
            return;
        }

        setGallery(newImages.slice(0, 12));
    };

    const handleUpload = async () => {
        try {
            if (newGallery.length + gallery.length == 0) {
                alert('Pleace upload aleast one image for the gallery');
                return;
            }

            // toast.loading('Uploading...'); // set upload status to uploading

            const formData = new FormData(); // create a new FormData object

            formData.append('main_image', main_image[0]); // add each image to the form data

            formData.append('orginalMain_image', item.main_image);
            formData.append('description', descriptionRef.current.value);
            formData.append('food_title', food_titleRef.current.value);
            formData.append('ingredient', ingredientRef.current.value);
            formData.append('processofmaking', processofmakingRef.current.value);
            formData.append('status', "false");
            formData.append('price', priceRef.current.value);
            formData.append('category', selectedCategory); // add the description to the form data

            gallery.forEach((image) => {
                formData.append('gallery', image); // add each image to the form data
            });
            newGallery.forEach((image) => {
                formData.append('newGallery', image); // add each image to the form data
            });

            // make the API call using axios
            const response = await axios.post(`http://localhost:5000/food/update_food_detail/${item.id}`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });

            setUploadStatus('Upload successful!'); // set upload status to successful
            toast.success('Updated successful!');


        } catch (error) {
            console.error(error);
            setUploadStatus('Upload failed. Please try again.');
            toast.error('Filled up all the field!');
        }
    };

    const { getRootProps: getRootProps1 } = useDropzone({ onDrop: handleDrop1 });
    const { getRootProps: getRootProps2 } = useDropzone({ onDrop: handleDrop2 });

    const [selectedCategory, setselectedCategory] = useState(item.category);

    const handleCategory = (event) => {

        setselectedCategory(event.target.value);

    };

    const [canUpload, setCanUpload] = useState(false); // add a state for button status

    // add useEffect to update button status
    useEffect(() => {
        if (gallery.length > 0) {
            setCanUpload(true);
        } else {
            setCanUpload(false);
        }
    }, [gallery, main_image, selectedCategory]);


    // even handler
    const handleSubmit = (event) => {
        event.preventDefault(); // Prevent default form submission behavior
    };

    // new updated image array
    const [newGallery, setNewGallery] = useState(item.gallery);

    function deleteImage(image) {

        try {

            // Create a new array that excludes the item with the matching id
            const updatedGallery = newGallery.filter((item) => item !== image);

            // Call the API or perform any other necessary operations to delete the item from the database or storage
            // ...

            // Update the state with the new gallery array
            setNewGallery(updatedGallery);
        } catch (error) {
            console.error(error);
        }
    };


    return (
        <div>
            <div class="l-navbar" id="bx-x">
                <nav class="sidebar">
                    <img className="logo" src={logo} alt="logo" width={"110px"} height={"110px"} />
                    <div class="menu-bar">
                        <div class="menu">
                            <hr></hr>
                            <ul class="menu-links">
                                <li class="nav-link">
                                    <Link to="/dashboard">
                                        <i class='bx bx-home-alt icon' ></i>
                                        <span class="text nav-text">Dashboard</span>
                                    </Link>
                                </li>

                                <li class="nav-link">
                                    <Link to="/room">
                                        <i class='bx bx-bar-chart-alt-2 icon' ></i>
                                        <span class="text nav-text">Room</span>
                                    </Link>
                                </li>

                                <li class="nav-link">
                                    <Link className="room" to="/food">
                                        <i class='bx bx-bell icon'></i>
                                        <span class="text nav-text">Food</span>
                                    </Link>
                                </li>

                                <li class="nav-link">
                                    <Link to="/food">
                                        <i class='bx bx-pie-chart-alt icon' ></i>
                                        <span class="text nav-text">Customer</span>
                                    </Link>
                                </li>

                                <li class="nav-link">
                                    <Link to="/contant">
                                        <i class='bx bx-heart icon' ></i>
                                        <span class="text nav-text">Contact</span>
                                    </Link>
                                </li>

                                <li class="nav-link">
                                    <Link to="/money">
                                        <i class='bx bx-wallet icon' ></i>
                                        <span class="text nav-text">Money</span>
                                    </Link>
                                </li>

                            </ul>
                        </div>

                        <div class="bottom-content">

                            <li class="logout">
                                <a href="#" onClick={e => logout(e)}>
                                    <i class='bx bx-log-out icon' ></i>
                                    <span class="text nav-text">Logout</span>
                                </a>
                            </li>

                        </div>
                    </div>

                </nav>

            </div>
            <section class="home">
                <div class="top-bar sticky-top">
                    <div class=" align-items-center">
                        <div class="col-md-6">
                            <div class="form">
                                <i class='bx bx-search icon'></i>
                                <input type="text" className="form-input form-control" placeholder="Search for people" />
                            </div>
                        </div>
                    </div>
                    <h2 class="text">Welcome {name}</h2>
                    <h2 class="text">{date}</h2>
                    <h2 class="text">{time}</h2>
                </div>

                <div className="content">

                    <Link to={"/food/update"} className="position-fixed">
                        <FontAwesomeIcon icon={faCircleArrowLeft} bounce size="xl" style={{ color: "#445c4c" }} />
                    </Link>
                    <div className="roomBook d-flex">
                        <div className="photo ">
                            <div className="main-photo" style={{ width: "650px", height: "400px" }}>
                                <img src={image} {...getRootProps1()} class="rounded text-center mx-auto d-block" style={{ opacity: "0.6", width: "300px", cursor: "pointer" }} />
                                {imageStatus == 0 ? <div key={item.id}>
                                    <ModalImage
                                        small={`http://localhost:5000/food/${item.main_image}`}
                                        large={`http://localhost:5000/food/${item.main_image}`}
                                        className="image1 mainImage rounded text-center  mx-auto d-block"
                                        style={{ cursor: 'pointer' }}
                                    />

                                </div> : <div>{main_image.length > 0 && (
                                    <div key={main_image[0].name}>
                                        <ModalImage
                                            small={URL.createObjectURL(main_image[0])}
                                            large={URL.createObjectURL(main_image[0])}
                                            alt={main_image[0].name}
                                            className="image1 rounded text-center  mx-auto d-block"
                                            style={{ cursor: 'pointer', }}
                                        />
                                    </div>
                                )}</div>}


                            </div>


                            <div className="gallery d-flex flex-wrap">

                                {newGallery.map((image, index) => (
                                    <div
                                        key={item.id}
                                        className="img-fluid"
                                        style={{ width: "200px", padding: "5px", }}>
                                        <div>
                                            <ModalImage
                                                small={`http://localhost:5000/food/${image}`}
                                                large={`http://localhost:5000/food/${image}`}

                                                className="image2 align-self-start img-fluid"
                                                style={{ cursor: 'pointer', }}


                                            />

                                            <i onClick={() => { deleteImage(image) }} class="bi bi-trash3-fill d-flex justify-content-end p-2" style={{ marginTop: "-40px", cursor: 'pointer', color: 'red', fontSize: "22px" }} ></i>

                                        </div>


                                    </div>
                                ))}
                                {gallery.map((image, index) => (
                                    <div
                                        key={image.name}
                                        className="img-fluid"
                                        style={{ width: "200px", padding: "5px", }}>
                                        <div>
                                            <ModalImage
                                                small={URL.createObjectURL(image)}
                                                large={URL.createObjectURL(image)}
                                                alt={image.name}
                                                className="image2 align-self-start img-fluid"
                                                style={{ cursor: 'pointer', }}
                                            />
                                        </div>

                                    </div>
                                ))}
                                <img src={gallery_icon} {...getRootProps2()} class="rounded mx-auto d-block" style={{ opacity: "0.6", width: "200px", height: "200px", cursor: "pointer", }} ></img>
                            </div>
                        </div>
                        <div />


                        <form onSubmit={handleSubmit} className="booking">
                            <label for="foodTitle">Food Title</label><br />
                            <input type="text" ref={food_titleRef} defaultValue={item.food_title} placeholder="ENTER Food TITLE" id="foodTitle" name="foodTitle" required /><br /><br />

                            <label for="price">Price</label><br />
                            <input type="number" ref={priceRef} defaultValue={item.price} placeholder="ENTER food PRICE" id="price" name="price" required /><br /><br />
                            <br />
                            <label for="description">Description</label><br />
                            <textarea style={{ height: "120px" }} id="description" defaultValue={item.description} placeholder="WRITE FOODDESCRIPTION" ref={descriptionRef} name="description" required></textarea><br /><br />

                            <label for="ingredient">Ingredient</label><br />
                            <textarea style={{ height: "120px" }} id="ingredient" defaultValue={item.ingredient} placeholder="WRITE FOOD INGREDIENT" ref={ingredientRef} name="ingredient" required></textarea><br /><br />

                            <label for="processOfMaking">Process of making</label><br />
                            <textarea style={{ height: "120px" }} id="processOfMaking" defaultValue={item.processofmaking} placeholder="WRITE STEP TO MAKE" ref={processofmakingRef} name="processOfMaking" required></textarea><br /><br />

                            <label for="foodCategory">Food Category</label><br />
                            <select id="foodCategory" placeholder="SELECT FOOD CATEGORY" value={selectedCategory} onChange={handleCategory} name="foodCategory">
                                <option value="vegetarian">Vegetarian</option>
                                <option value="non-vegetarian">Non-vegetarian</option>
                                <option value="vegan">Vegan</option>
                                <option value="drink">Drinks</option>
                            </select><br /><br />

                            <button className="addBotton" type="submit"
                                onClick={handleUpload}>Update
                            </button>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default EditFood;
