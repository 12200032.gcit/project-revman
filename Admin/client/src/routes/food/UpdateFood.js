import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { Link } from 'react-router-dom';
import "../../css/sidebar.css";
import "../../css/room.css";
import logo from "../../assets/logo.png";
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

const UpdateFood = ({ setAuth }) => {
  const [name, setName] = useState("");

  const getProfile = async () => {
    try {
      const res = await fetch("http://localhost:5000/dashboard/", {
        method: "POST",
        headers: { revman_token: localStorage.token }
      });

      const parseData = await res.json();
      setName(parseData.user_name);
    } catch (err) {
      console.error(err.message);
    }
  };

  const logout = async e => {
    e.preventDefault();
    try {
      localStorage.removeItem("token");
      setAuth(false);
      toast.success("Logout successfully");
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getProfile();
  }, []);

  // Date and Time
  setInterval(updateTime, 1000);
  const now = new Date().toLocaleTimeString();
  const nowDate = new Date().toDateString();

  const [time, setTime] = useState(now);
  const [date, setDate] = useState(nowDate);

  function updateTime() {
    const newTime = new Date().toLocaleTimeString();
    const newDate = new Date().toDateString();
    setDate(newDate);
    setTime(newTime);
  }

  // get data
  const [category, setCategory] = useState('vegetarian');
  const [data, setData] = useState([]);


  // handle category

  function handleCategory(category) {
    setCategory(category);
  }

  useEffect(() => {
    axios.get(`http://localhost:5000/food/update/${category}`)
      .then(res => {
        setData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [category,data]);

  // delete room


  const deleteFood = async (food_id) => {
    try {
      confirmAlert({
        title: 'Confirm',
        message: 'Are you sure you want to delete this food item?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              axios.delete(`http://localhost:5000/food/delete/${food_id}`);
              toast.success('Delete successfully');
            },
          },
          {
            label: 'No',
            onClick: () => { },
          },
        ],
      });

    } catch (err) {
      console.error(err.message);
    }
  };


  return (
    <div>
      <div class="l-navbar" id="bx-x">
        <nav class="sidebar">
          <img className="logo" src={logo} alt="logo" width={"110px"} height={"110px"} />
          <div class="menu-bar">
            <div class="menu">
              <hr></hr>
              <ul class="menu-links">
                <li class="nav-link">
                  <Link to="/dashboard">
                    <i class='bx bx-home-alt icon' ></i>
                    <span class="text nav-text">Dashboard</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/room">
                    <i class='bx bx-bar-chart-alt-2 icon' ></i>
                    <span class="text nav-text">Room</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link className="dashboard" to="/food">
                    <i class='bx bx-bell icon'></i>
                    <span class="text nav-text">Food</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/food">
                    <i class='bx bx-pie-chart-alt icon' ></i>
                    <span class="text nav-text">Customer</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/contant">
                    <i class='bx bx-heart icon' ></i>
                    <span class="text nav-text">Contact</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/money">
                    <i class='bx bx-wallet icon' ></i>
                    <span class="text nav-text">Money</span>
                  </Link>
                </li>

              </ul>
            </div>

            <div class="bottom-content">

              <li class="logout">
                <a href="#" onClick={e => logout(e)}>
                  <i class='bx bx-log-out icon' ></i>
                  <span class="text nav-text">Logout</span>
                </a>
              </li>

            </div>
          </div>

        </nav>

      </div>
      <section class="home">
        <div class="top-bar sticky-top">
          <div class=" align-items-center">
            <div class="col-md-6">
              <div class="form">
                <i class='bx bx-search icon'></i>
                <input type="text" class="form-control form-input" placeholder="Search for people" />
              </div>
            </div>
          </div>
          <h4 class="text">Welcome {name}</h4>
          <h4 class="text">{date}</h4>
          <h4 class="text">{time}</h4>
        </div>

        <div className="content">
          <Link to={"/food"} className="position-fixed">
            <FontAwesomeIcon icon={faCircleArrowLeft} bounce size="xl" style={{ color: "#445c4c", zIndex: "100" }} />
          </Link>
          <div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <button class="nav-link active" data-bs-toggle="tab" onClick={() => { handleCategory('vegetarian') }} data-bs-target="#single" type="button" role="tab" aria-controls="single" aria-selected="true">Vegetarian</button>
              </li>
              <li class="nav-item" role="presentation">
                <button class="nav-link" data-bs-toggle="tab" onClick={() => { handleCategory('non-vegetarian') }} data-bs-target="#double" type="button" role="tab" aria-controls="double" aria-selected="false">Non-vegetarian</button>
              </li>
              <li class="nav-item" role="presentation">
                <button class="nav-link" data-bs-toggle="tab" onClick={() => { handleCategory('vegan') }} data-bs-target="#triple" type="button" role="tab" aria-controls="triple" aria-selected="false">Vegan</button>
              </li>
              <li class="nav-item" role="presentation">
                <button class="nav-link" data-bs-toggle="tab" onClick={() => { handleCategory('drink') }} data-bs-target="#quad" type="button" role="tab" aria-controls="quad" aria-selected="false">Drinks</button>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane  show active" id="single" role="tabpanel" aria-labelledby="single-tab">
                <table>
                  <tr className="tableHead">
                    <th>SL No</th>
                    <th>Food Name</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Ingredient</th>
                    <th>Process of making</th>
           
                  </tr>
                  <hr style={{ width: "900%", }} />
                  {data.map((item, index) => (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td className="text-truncate">{item.food_title}</td>
                      <td>{item.price}</td>
                      <td><div className="text-truncate">{item.description}</div></td>
                      <td><div className="text-truncate">{item.ingredient}</div></td>
                      <td><div className="text-truncate">{item.processofmaking}</div></td>
                      <td><Link to="/food/update/edit" state={item}><i class="bi bi-pen-fill" ></i></Link></td>
                      <td><i class="bi bi-trash3-fill" onClick={() => {deleteFood(item.id) }}></i></td>
                    </tr>

                  ))}
                </table>
              </div>
              <div class="tab-pane " id="double" role="tabpanel" aria-labelledby="double-tab">
              <table>
                  <tr className="tableHead">
                    <th>SL No</th>
                    <th>Food Name</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Ingredient</th>
                    <th>Process of making</th>
           
                  </tr>
                  <hr style={{ width: "900%", }} />
                  {data.map((item, index) => (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td className="text-truncate">{item.food_title}</td>
                      <td>{item.price}</td>
                      <td><div className="text-truncate">{item.description}</div></td>
                      <td><div className="text-truncate">{item.ingredient}</div></td>
                      <td><div className="text-truncate">{item.processofmaking}</div></td>
                      <td><Link to="/food/update/edit" state={item}><i class="bi bi-pen-fill" ></i></Link></td>
                      <td><i class="bi bi-trash3-fill" onClick={() => {deleteFood(item.id) }}></i></td>
                    </tr>

                  ))}
                </table>
              </div>
              <div class="tab-pane " id="triple" role="tabpanel" aria-labelledby="triple-tab">
              <table>
                  <tr className="tableHead">
                    <th>SL No</th>
                    <th>Food Name</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Ingredient</th>
                    <th>Process of making</th>
           
                  </tr>
                  <hr style={{ width: "900%", }} />
                  {data.map((item, index) => (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td className="text-truncate">{item.food_title}</td>
                      <td>{item.price}</td>
                      <td><div className="text-truncate">{item.description}</div></td>
                      <td><div className="text-truncate">{item.ingredient}</div></td>
                      <td><div className="text-truncate">{item.processofmaking}</div></td>
                      <td><Link to="/food/update/edit" state={item}><i class="bi bi-pen-fill" ></i></Link></td>
                      <td><i class="bi bi-trash3-fill" onClick={() => {deleteFood(item.id) }}></i></td>
                    </tr>

                  ))}
                </table>
              </div>
              <div class="tab-pane" id="quad" role="tabpanel" aria-labelledby="quad-tab">
              <table>
                  <tr className="tableHead">
                    <th>SL No</th>
                    <th>Food Name</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Ingredient</th>
                    <th>Process of making</th>
           
                  </tr>
                  <hr style={{ width: "900%", }} />
                  {data.map((item, index) => (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td className="text-truncate">{item.food_title}</td>
                      <td>{item.price}</td>
                      <td><div className="text-truncate">{item.description}</div></td>
                      <td><div className="text-truncate">{item.ingredient}</div></td>
                      <td><div className="text-truncate">{item.processofmaking}</div></td>
                      <td><Link to="/food/update/edit" state={item}><i class="bi bi-pen-fill" ></i></Link></td>
                      <td><i class="bi bi-trash3-fill" onClick={() => {deleteFood(item.id) }}></i></td>
                    </tr>
                  ))}
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );

};

export default UpdateFood;
{/* <a href="#" onClick={e => logout(e)} class="nav_link"> <i class='bx bx-log-out nav_icon'></i> <span class="nav_name">SignOut</span> </a> */ }