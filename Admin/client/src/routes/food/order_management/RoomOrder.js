import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { Link } from 'react-router-dom';
import "../../../css/sidebar.css";
import "../../../css/room.css";
import logo from "../../../assets/logo.png";
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

const TableOrder = ({ setAuth }) => {
  const [name, setName] = useState("");

  const getProfile = async () => {
    try {
      const res = await fetch("http://localhost:5000/dashboard/", {
        method: "POST",
        headers: { revman_token: localStorage.token }
      });

      const parseData = await res.json();
      setName(parseData.user_name);
    } catch (err) {
      console.error(err.message);
    }
  };

  const logout = async e => {
    e.preventDefault();
    try {
      localStorage.removeItem("token");
      setAuth(false);
      toast.success("Logout successfully");
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getProfile();
  }, []);

  // Date and Time
  setInterval(updateTime, 1000);
  const now = new Date().toLocaleTimeString();
  const nowDate = new Date().toDateString();

  const [time, setTime] = useState(now);
  const [date, setDate] = useState(nowDate);

  function updateTime() {
    const newTime = new Date().toLocaleTimeString();
    const newDate = new Date().toDateString();
    setDate(newDate);
    setTime(newTime);
  }

  // get data
  const [data, setData] = useState([]);
  const [vipData, setVipData] = useState([]);
  const [dailyData, setDailyData] = useState([]);

  // handle category

  useEffect(() => {
    axios.get(`http://localhost:5000/food/room_order`)
      .then(res => {
        setData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [data]);

  useEffect(() => {
    axios.get(`http://localhost:5000/food/vip_room_order`)
      .then(res => {
        setVipData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [data]);

  useEffect(() => {
    axios.get(`http://localhost:5000/food/daily_room_orders`)
      .then(res => {
        setDailyData(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [data]);
  // delete room


  const deleteOrder = async (food_id) => {
    try {
      confirmAlert({
        title: 'Confirm',
        message: 'Are you sure you want to reject the order?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              axios.delete(`http://localhost:5000/food/rejectOrder/${food_id}`);
              toast.success('Order rejected successfully');
            },
          },
          {
            label: 'No',
            onClick: () => { },
          },
        ],
      });

    } catch (err) {
      console.error(err.message);
    }
  };

  // confirmRequest
  const orderConfirm = async (id) => {
    try {
      confirmAlert({
        title: 'Confirm',
        message: 'Are you sure you want to confirm the order?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              axios.post(`http://localhost:5000/food/orderConfirm/${id}`);
              toast.success('Order confirm successfully');
            },
          },
          {
            label: 'No',
            onClick: () => { },
          },
        ],
      });

    } catch (err) {
      console.error(err.message);
    }
  };
  // vipOrder
  const vipOrder = async (id) => {
    try {
      confirmAlert({
        title: 'Confirm',
        message: 'Are you sure you want to sent to the Vip orders?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              axios.post(`http://localhost:5000/food/vip_order/${id}`);
              toast.success('Successfully sent');
            },
          },
          {
            label: 'No',
            onClick: () => { },
          },
        ],
      });

    } catch (err) {
      console.error(err.message);
    }
  };

  const [id, setId] = useState();
  const [active, setActive] = useState();
  function setNewId(id, bar) {
    setId(id);
    setActive(bar);
  }


  return (
    <div>
      <div class="l-navbar" id="bx-x">
        <nav class="sidebar">
          <img className="logo" src={logo} alt="logo" width={"110px"} height={"110px"} />
          <div class="menu-bar">
            <div class="menu">
              <hr></hr>
              <ul class="menu-links">
                <li class="nav-link">
                  <Link to="/dashboard">
                    <i class='bx bx-home-alt icon' ></i>
                    <span class="text nav-text">Dashboard</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/room">
                    <i class='bx bx-bar-chart-alt-2 icon' ></i>
                    <span class="text nav-text">Room</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link className="dashboard" to="/food">
                    <i class='bx bx-bell icon'></i>
                    <span class="text nav-text">Food</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/food">
                    <i class='bx bx-pie-chart-alt icon' ></i>
                    <span class="text nav-text">Customer</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/contant">
                    <i class='bx bx-heart icon' ></i>
                    <span class="text nav-text">Contact</span>
                  </Link>
                </li>

                <li class="nav-link">
                  <Link to="/money">
                    <i class='bx bx-wallet icon' ></i>
                    <span class="text nav-text">Money</span>
                  </Link>
                </li>

              </ul>
            </div>

            <div class="bottom-content">

              <li class="logout">
                <a href="#" onClick={e => logout(e)}>
                  <i class='bx bx-log-out icon' ></i>
                  <span class="text nav-text">Logout</span>
                </a>
              </li>

            </div>
          </div>

        </nav>

      </div>
      <section class="home">
        <div class="top-bar sticky-top">
          <div class=" align-items-center">
            <div class="col-md-6">
              <div class="form">
                <i class='bx bx-search icon'></i>
                <input type="text" class="form-control form-input" placeholder="Search for people" />
              </div>
            </div>
          </div>
          <h4 class="text">Welcome {name}</h4>
          <h4 class="text">{date}</h4>
          <h4 class="text">{time}</h4>
        </div>

        <div className="content">
          <Link to={"/food"} className="position-fixed">
            <FontAwesomeIcon icon={faCircleArrowLeft} bounce size="xl" style={{ color: "#445c4c", zIndex: "100" }} />
          </Link>
          <div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#single" type="button" role="tab" aria-controls="single" aria-selected="true">Orders</button>
              </li>
              <li class="nav-item" role="presentation">
                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#double" type="button" role="tab" aria-controls="double" aria-selected="false">VIP orders</button>
              </li>
              <li class="nav-item" role="presentation">
                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#triple" type="button" role="tab" aria-controls="triple" aria-selected="false">daily order review</button>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane  show active" id="single" role="tabpanel" aria-labelledby="single-tab">
                <table>
                  <tr className="tableHead">
                    <th>SL No</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Room code</th>
                    <th>Food Items</th>
                    <th>message</th>

                  </tr>
                  <hr style={{ width: "900%", }} />
                  {data.map((item, index) => (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td className="text-truncate">{item.name}</td>
                      <td><div className="text-truncate">{item.phone_number}</div></td>
                      <td><div className="text-truncate">{item.room_code}</div></td>
                      <td><div className="text-truncate">1.{item.order_food[0]}
                        <br />2.{item.order_food[1]}</div></td>
                      <td><div className="text-truncate">{item.message}</div></td>
                      <td><i onClick={() => { orderConfirm(item.id) }} class='bx bxs-check-circle'></i></td>
                      <td><i onClick={() => { deleteOrder(item.id) }} class='bx bxs-x-circle'></i></td>
                      <td><i onClick={() => { vipOrder(item.id) }} class='bi bi-send-fill'></i></td>
                      <td><i onClick={() => { setNewId(item.id, "orders") }} data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="bi bi-info-circle-fill info"></i></td>
                    </tr>

                  ))}
                </table>
              </div>
              <div class="tab-pane " id="double" role="tabpanel" aria-labelledby="double-tab">
                <table>
                  <tr className="tableHead">
                    <th>SL No</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Room code</th>
                    <th>Food Items</th>
                    <th>message</th>

                  </tr>
                  <hr style={{ width: "900%", }} />
                  {vipData.map((item, index) => (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td className="text-truncate">{item.name}</td>
                      <td><div className="text-truncate">{item.phone_number}</div></td>
                      <td><div className="text-truncate">{item.room_code}</div></td>
                      <td><div className="text-truncate">1.{item.order_food[0]}
                        <br />2.{item.order_food[1]}</div></td>
                      <td><div className="text-truncate">{item.message}</div></td>
                      <td><i onClick={() => { orderConfirm(item.id) }} class='bx bxs-check-circle'></i></td>
                      <td><i onClick={() => { deleteOrder(item.id) }} class='bx bxs-x-circle'></i></td>
                      <td><i onClick={() => { setNewId(item.id, "vip_order") }} data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="bi bi-info-circle-fill info"></i></td>
                    </tr>

                  ))}
                </table>
              </div>
              <div class="tab-pane " id="triple" role="tabpanel" aria-labelledby="triple-tab">
                <table>
                  <tr className="tableHead">
                    <th>SL No</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Room code</th>
                    <th>Food Items</th>
                    <th>message</th>

                  </tr>
                  <hr style={{ width: "900%", }} />
                  {dailyData.map((item, index) => (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td className="text-truncate">{item.name}</td>
                      <td><div className="text-truncate">{item.phone_number}</div></td>
                      <td><div className="text-truncate">{item.room_code}</div></td>
                      <td><div className="text-truncate">1.{item.order_food[0]}
                        <br />2.{item.order_food[1]}</div></td>
                      <td><div className="text-truncate">{item.message}</div></td>
                      <td><i onClick={() => { setNewId(item.id, "daily_order") }} data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="bi bi-info-circle-fill info"></i></td>
                    </tr>

                  ))}
                </table>
              </div>
            </div>
          </div>
        </div>
        {/* detail information*/}
        <div class="modal " id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content" style={{ width: "150%", marginLeft: "-70px" }}>
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-dialog modal-dialog-scrollable">
                {active === "orders" &&
                  data.map((item, index) => {
                    if (item.id === id) {
                      return (
                        <div>
                          <div>Name: {item.name}</div>
                          <div>Phone Number:{item.phone_number}</div>
                          <div>Room Code:{item.room_code}</div>
                          <div>
                            Food Items:
                            {item.order_food.map((food, index) => (
                              <div key={index}>
                                {`${index + 1}. ${food}`}
                              </div>
                            ))}
                          </div>

                          <div>Message: {item.message}</div>
                          <div>OrderTime: {new Date(item.booking_time).toLocaleTimeString()}</div>
                        </div>
                      );
                    }
                    return null;
                  })
                }
                {active === "vip_order" &&
                  vipData.map((item, index) => {
                    if (item.id === id) {
                      return (
                        <div>
                          <div>Name: {item.name}</div>
                          <div>Phone Number:{item.phone_number}</div>
                          <div>Room Code:{item.room_code}</div>
                          <div>
                            Food Items:
                            {item.order_food.map((food, index) => (
                              <div key={index}>
                                {`${index + 1}. ${food}`}
                              </div>
                            ))}
                          </div>

                          <div>Message: {item.message}</div>
                          <div>Order Time: {new Date(item.booking_time).toLocaleTimeString()}</div>
                        </div>
                      );
                    }
                    return null;
                  })
                }
                {active === "daily_order" &&
                  dailyData.map((item, index) => {
                    if (item.id === id) {
                      return (
                        <div>
                          <div>Name: {item.name}</div>
                          <div>Phone Number:{item.phone_number}</div>
                          <div>Room Code:{item.room_code}</div>
                          <div>
                            Food Items:
                            {item.order_food.map((food, index) => (
                              <div key={index}>
                                {`${index + 1}. ${food}`}
                              </div>
                            ))}
                          </div>

                          <div>Message: {item.message}</div>
                          <div>Order Time: {new Date(item.booking_time).toLocaleTimeString()}</div>
                        </div>
                      );
                    }
                    return null;
                  })
                }

              </div>

              <div class="modal-footer">
                {active === "orders" && (
                  <>
                    <button onClick={() => { orderConfirm(id) }} data-bs-dismiss="modal" type="button" class="btn btn-primary">
                      Confirm
                    </button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                      Cancel
                    </button>
                  </>
                )}
                {active === "vip_order" && (
                  <>
                    <button onClick={() => { orderConfirm(id) }} data-bs-dismiss="modal" type="button" class="btn btn-primary">
                      Confirm
                    </button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                      Cancel
                    </button>
                  </>
                )}

                {active === "daily_order" && (
                  <>

                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                      close
                    </button>
                  </>
                )}


              </div>

            </div>
          </div>
        </div>
      </section>
    </div>
  );

};

export default TableOrder;
{/* <a href="#" onClick={e => logout(e)} class="nav_link"> <i class='bx bx-log-out nav_icon'></i> <span class="nav_name">SignOut</span> </a> */ }