const express = require('express');
const { Pool } = require('pg');
const session = require('express-session');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const cors = require('cors');
const router = express.Router();
const pool = require("../db");


// const cookieSession = require('cookie-session');
const app = express();

// Serve static files from the "uploads" directory
router.use('/uploads', express.static('uploads'))

pool.query(
  `CREATE TABLE IF NOT EXISTS room (
        id SERIAL PRIMARY KEY,
        main_image TEXT[],
        gallery TEXT[],
        room_title VARCHAR(255) NOT NULL,
        room_code VARCHAR(255) NOT NULL UNIQUE,
        price INTEGER NOT NULL,
        capacity_adult INTEGER NOT NULL,
        capacity_child INTEGER NOT NULL,
        description TEXT NOT NULL,
        category VARCHAR(50),
        status VARCHAR(10) NOT NULL,
        rating FLOAT,
        no_of_people INTEGER,
        feedback TEXT[]
  )`,
  (err, res) => {
    if (err) {
      console.log(err);
    } else {
      console.log('room table created succesfully');
    }
  }
);


const multer = require('multer');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

const fileFilter = function (req, file, cb) {
  const allowedMimes = ['image/jpeg', 'image/png', 'image/jpg', 'image/webp', 'image/gif'];
  if (allowedMimes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(new Error('Invalid file type. Only JPEG, PNG, and GIF files are allowed.'), false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 20 // 20 MB
  },
  fileFilter: fileFilter,
}).fields([{ name: 'gallery', maxCount: 20 }, { name: 'main_image', maxCount: 1 }]);

// Handle file upload
router.post('/upload', function (req, res) {
  upload(req, res, async function (err) {
    if (err) {
      // An error occurred when uploading
      console.log(err);
      return res.status(400).json({ message: 'Error uploading file.' });
    }

    // Everything went fine

    try {
      const {
        room_title,
        room_code,
        price,
        capacity_adult,
        capacity_child,
        description,
        category,
        status } = req.body;
      // const postBy = 'user'; // change this to the logged-in user

      // const filepaths = req.files.map(file => file.path); // get the image paths
      const filepaths = req.files['gallery'].map(file => file.path);
      const filepaths2 = req.files['main_image'].map(file => file.path);

      // insert the post data into the database
      const { rows } = await pool.query(
        'INSERT INTO room (main_image,gallery,room_title,room_code,price,capacity_adult,capacity_child,description,category,status) VALUES ($1, $2, $3, $4, $5, $6 ,$7,$8,$9,$10) RETURNING *',
        [filepaths2, filepaths, room_title, room_code, price, capacity_adult, capacity_child, description, category, status]

      );

      res.status(201).json(rows[0]);
    } catch (err) {
      console.error(err);
      res.status(500).json({ message: 'Server Error' });
    }
  });
});

router.get('/update/:category', async (req, res) => {

  const category = req.params.category;

  pool.query(`SELECT * FROM room where category = '${category}'`, (error, result) => {
    if (error) {
      throw error;
    }
    res.send(result.rows);

  });
});

router.delete('/delete/:roomId', async (req, res) => {
  const roomId = req.params.roomId;

  // Delete the comment from the database
  const query = 'DELETE FROM room WHERE id = $1';
  await pool.query(query, [roomId]);

  res.json({ message: 'Comment deleted successfully' });
});


router.post('/updateRoomDetail/:room_Id', async (req, res) => {
  upload(req, res, async function (err) {
    if (err) {
      // An error occurred when uploading
      console.log(err);
      return res.status(400).json({ message: 'Error uploading file.' });
    }

    try {
      const room_Id = req.params.room_Id;
      const {
        room_title,
        room_code,
        price,
        capacity_adult,
        capacity_child,
        description,
        category,
        status,
        orginalMain_image,
        newGallery
      } = req.body;

      let newArrayGallery = [];
      if (req.files['gallery'] != null) {
        const filepaths = req.files['gallery'].map(file => file.path);
        newArrayGallery = [newGallery, ...filepaths].flat();
      } else {
        newArrayGallery = [newGallery].flat();
      }

      let newMain_image = '';
      if (req.files['main_image'] != null) {
        const filepaths2 = req.files['main_image'].map(file => file.path);
        newMain_image = filepaths2;
      } else {
        const pathArray = orginalMain_image.split("\\");
        const newPath = pathArray.join("\\\\");
        newMain_image = `{ "${newPath}" }`;
      }

      newArrayGallery = newArrayGallery.filter(item => item !== undefined);

      const query = `UPDATE room SET 
                      main_image = $1,
                      room_title = $2, 
                      room_code = $3,
                      price = $4,
                      capacity_adult = $5,
                      capacity_child = $6,
                      description = $7,
                      category = $8,
                      status = $9,
                      gallery = $10
                    WHERE id = $11`;

      await pool.query(query, [
        newMain_image,
        room_title,
        room_code,
        price,
        capacity_adult,
        capacity_child,
        description,
        category,
        status,
        newArrayGallery,
        room_Id
      ]);

      res.json({ message: 'room update successfully successfully' });

    } catch (error) {
      console.log(error);
      res.status(500).send('Error');
    }
  })
});


module.exports = router;