const express = require('express');
const { Pool } = require('pg');
const session = require('express-session');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const cors = require('cors');
const router = express.Router();
const pool = require("../db");

// const cookieSession = require('cookie-session');
const app = express();


pool.query(
  `CREATE TABLE IF NOT EXISTS customer (
        id SERIAL PRIMARY KEY,
        room_code VARCHAR(255),
        table_code VARCHAR(255),
        order_food TEXT[],
        name VARCHAR(20) NOT NULL,
        phone_number INT,
        email VARCHAR(50),
        check_in TIMESTAMP WITH TIME ZONE,
        check_out TIMESTAMP WITH TIME ZONE,
        message TEXT,
        booking_time TIMESTAMP WITH TIME ZONE,
        confirming_time TIMESTAMP WITH TIME ZONE,
        status TEXT DEFAULT 'false'
  )`,
  (err, res) => {
    if (err) {
      console.log(err);
    } else {
      console.log('customer table created succesfully');
    }
  }
);

router.post('/order_food', async (req, res) => {

  try {
    const { table_code, name, phone_number,food_items, message } = req.body;

    // post to database
    const { rows } = await pool.query(
      'INSERT INTO customer (room_code,name,phone_number,order_food,message,booking_time) VALUES ($1, $2, $3, $4, $5,CURRENT_TIMESTAMP) RETURNING *',
      [table_code, name, phone_number,food_items, message]

    );

    res.status(201).json(rows[0]);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server Error' });
  }
});


router.get('/pandingDetail', async (req, res) => {

  try {
    pool.query(`SELECT * FROM customer where status = 'true'`, (error, result) => {
      if (error) {
        throw error;
      }
      res.send(result.rows);
    });

  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server Error' });
  }
});

router.post('/confirmRequest/:customerId', async (req, res) => {
  const customerId = req.params.customerId;

  const confirm = "confirm";
  const now = new Date();
  // Update the status of the customer in the database
  const query = `UPDATE customer SET status = $1,confirming_time = $2 WHERE id = $3`;
  await pool.query(query, [confirm, now, customerId]);


});

router.post('/checkOutRequest/:checkOutId', async (req, res) => {
  const checkOutId = req.params.checkOutId;
  const check_out = "check-out";
  const now = new Date();
  // Update the status of the customer in the database
  const query = `UPDATE customer SET status = $1,check_out = $2 WHERE id = $3`;
  await pool.query(query, [check_out, now, checkOutId]);


});


router.delete('/rejectRequest/:customerId', async (req, res) => {
  const customerId = req.params.customerId;

  // Delete the comment from the database
  const query = 'DELETE FROM customer WHERE id = $1';
  await pool.query(query, [customerId]);

  res.json({ message: 'customer deleted successfully' });
});

router.get('/roomAvaibility', async (req, res) => {

  try {
    pool.query(`SELECT * FROM room where status = 'false'`, (error, result) => {
      if (error) {
        throw error;
      }
      res.send(result.rows);
    });

  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server Error' });
  }
});

router.get('/check_in_customer', async (req, res) => {

  try {
    pool.query(`SELECT * FROM customer where status = 'confirm'`, (error, result) => {
      if (error) {
        throw error;
      }
      res.send(result.rows);
    });

  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server Error' });
  }
});

router.get('/roomCustomer', async (req, res) => {

  try {
    pool.query(`SELECT * FROM customer where status = 'check-out'`, (error, result) => {
      if (error) {
        throw error;
      }
      res.send(result.rows);
    });

  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Server Error' });
  }
});



const checkInStatus = async () => {

  const currentTime = new Date();
  try {
    const { rows } = await pool.query('SELECT * FROM customer WHERE status = $1', ['true']);

    for (const row of rows) {
      const currentBookingTime = new Date(row.booking_time);
      const bookingTimeThreshold = new Date(currentBookingTime.getTime() + 5 * 60 * 1000);
      
      if (currentTime >= bookingTimeThreshold) {
        await pool.query(
          'UPDATE room SET status = $1 WHERE room_code = $2', ['false', row.room_code]
        );
        await pool.query(
          'DELETE FROM customer WHERE id = $1',
          [row.id]
        );
       
      }
    }
    

  } catch (error) {
    console.error(error);
  }
};

setInterval(checkInStatus, 60000); // check every minute


const updateStatus = async () => {
  const currentTime = new Date();
  const { rows } = await pool.query('SELECT * FROM customer WHERE status = $1', ['confirm']);

  for (const row of rows) {
    const checkOutTime = new Date(row.check_out);

    if (currentTime >= checkOutTime) {
      await pool.query('UPDATE customer SET status = $1 WHERE id = $2', ['check-out', row.id]);
      await pool.query(
        'UPDATE room SET status = $1 WHERE room_code = $2', ['false', row.room_code]
      );
    }
  }

};
// Schedule the status update every minute
setInterval(updateStatus, 60 * 1000);

module.exports = router;
