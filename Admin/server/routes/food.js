const express = require('express');
const { Pool } = require('pg');
const session = require('express-session');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const cors = require('cors');
const router = express.Router();
const pool = require("../db");

// const cookieSession = require('cookie-session');
const app = express();

// Serve static files from the "uploads" directory
router.use('/uploads', express.static('uploads'))

pool.query(
  `CREATE TABLE IF NOT EXISTS food (
        id SERIAL PRIMARY KEY,
        main_image TEXT[],
        gallery TEXT[],
        food_title VARCHAR(255) NOT NULL,
        price INTEGER NOT NULL,
        description TEXT NOT NULL,
        processOfMaking TEXT NOT NULL,
        ingredient TEXT NOT NULL,
        category VARCHAR(50),
        status TEXT DEFAULT 'false'
  )`,
  (err, res) => {
    if (err) {
      console.log(err);
    } else {
      console.log('food table created succesfully');
    }
  }
);

const multer = require('multer');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

const fileFilter = function (req, file, cb) {
  const allowedMimes = ['image/jpeg', 'image/png', 'image/jpg', 'image/webp', 'image/gif'];
  if (allowedMimes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(new Error('Invalid file type. Only JPEG, PNG, and GIF files are allowed.'), false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 20 // 20 MB
  },
  fileFilter: fileFilter,
}).fields([{ name: 'gallery', maxCount: 20 }, { name: 'main_image', maxCount: 1 }]);

// Handle file upload
router.post('/upload', function (req, res) {
  upload(req, res, async function (err) {
    if (err) {
      // An error occurred when uploading
      console.log(err);
      return res.status(400).json({ message: 'Error uploading file.' });
    }

    // Everything went fine

    try {
      const {
        food_title,
        price,
        description,
        ingredient,
        processOfMaking,
        category } = req.body;
      // const postBy = 'user'; // change this to the logged-in user

      // const filepaths = req.files.map(file => file.path); // get the image paths
      const filepaths = req.files['gallery'].map(file => file.path);
      const filepaths2 = req.files['main_image'].map(file => file.path);

      // insert the post data into the database
      const { rows } = await pool.query(
        'INSERT INTO food (main_image,gallery,food_title,price,description,ingredient,processOfMaking,category) VALUES ($1, $2, $3, $4, $5, $6 ,$7,$8) RETURNING *',
        [filepaths2, filepaths, food_title, price, description, ingredient, processOfMaking, category]

      );

      res.status(201).json(rows[0]);
    } catch (err) {
      console.error(err);
      res.status(500).json({ message: 'Server Error' });
    }
  });
});


router.get('/update/:category', async (req, res) => {

  const category = req.params.category;

  pool.query(`SELECT * FROM food where category = '${category}'`, (error, result) => {
    if (error) {
      throw error;
    }
    res.send(result.rows);

  });
});

router.delete('/delete/:foodId', async (req, res) => {
  const foodId = req.params.foodId;

  // Delete the comment from the database
  const query = 'DELETE FROM food WHERE id = $1';
  await pool.query(query, [foodId]);

  res.json({ message: 'Comment deleted successfully' });
})

router.post('/update_food_detail/:foodId', async (req, res) => {
  upload(req, res, async function (err) {
    if (err) {
      // An error occurred when uploading
      console.log(err);
      return res.status(400).json({ message: 'Error uploading file.' });
    }

    try {
      const foodId = req.params.foodId;
      const {
        orginalMain_image,
        newGallery,
        food_title,
        price,
        description,
        ingredient,
        processofmaking,
        category,
      } = req.body;

      let newArrayGallery = [];
      if (req.files['gallery'] != null) {
        const filepaths = req.files['gallery'].map(file => file.path);
        newArrayGallery = [newGallery, ...filepaths].flat();
      } else {
        newArrayGallery = [newGallery].flat();
      }

      let newMain_image = '';
      if (req.files['main_image'] != null) {
        const filepaths2 = req.files['main_image'].map(file => file.path);
        newMain_image = filepaths2;
      } else {
        const pathArray = orginalMain_image.split("\\");
        const newPath = pathArray.join("\\\\");
        newMain_image = `{ "${newPath}" }`;
      }

      newArrayGallery = newArrayGallery.filter(item => item !== undefined);


      const query = `UPDATE food SET 
                      main_image = $1,
                      food_title = $2, 
                      price = $3,
                      description = $4,
                      category = $5,
                      ingredient = $6,
                      processofmaking = $7,
                      gallery = $8
                    WHERE id = $9`;

      await pool.query(query, [
        newMain_image,
        food_title,
        price,
        description,
        category,
        ingredient,
        processofmaking,
        newArrayGallery,
        foodId
      ]);

      res.json({ message: 'room update successfully successfully' });

    } catch (error) {
      console.log(error);
      res.status(500).send('Error');
    }
  })
});

router.get('/table_order', async (req, res) => {

  pool.query(`SELECT * FROM customer WHERE table_code IS NOT NULL AND status = 'false' `, (error, result) => {
    if (error) {
      throw error;
    }
    res.send(result.rows);
  });
  
});

router.get('/room_order', async (req, res) => {

  pool.query(`SELECT * FROM customer WHERE room_code IS NOT NULL AND status = 'false' `, (error, result) => {
    if (error) {
      throw error;
    }
    res.send(result.rows);
  });
  
});

router.get('/vip_table_order', async (req, res) => {

  pool.query(`SELECT * FROM customer WHERE table_code IS NOT NULL AND status = 'vip_order' `, (error, result) => {
    if (error) {
      throw error;
    }
    res.send(result.rows);
  });
  
});

router.get('/vip_room_order', async (req, res) => {

  pool.query(`SELECT * FROM customer WHERE room_code IS NOT NULL AND status = 'vip_order' `, (error, result) => {
    if (error) {
      throw error;
    }
    res.send(result.rows);
  });
  
});

router.get('/daily_orders', async (req, res) => {

  pool.query(`SELECT * FROM customer WHERE table_code IS NOT NULL AND status = 'active' `, (error, result) => {
    if (error) {
      throw error;
    }
    res.send(result.rows);
  });
});

router.get('/daily_room_orders', async (req, res) => {

  pool.query(`SELECT * FROM customer WHERE room_code IS NOT NULL AND status = 'active' `, (error, result) => {
    if (error) {
      throw error;
    }
    res.send(result.rows);
  });
});

router.post('/orderConfirm/:customerId', async (req, res) => {
  const customerId = req.params.customerId;
  const confirm = "active";
  const now = new Date();
  // Update the status of the customer in the database
  const query = `UPDATE customer SET status = $1,confirming_time = $2 WHERE id = $3`;
  await pool.query(query, [confirm, now, customerId]);


});

router.post('/vip_order/:customerId', async (req, res) => {
  const customerId = req.params.customerId;
  const confirm = "vip_order";

  // Update the status of the customer in the database
  const query = `UPDATE customer SET status = $1 WHERE id = $2`;
  await pool.query(query, [confirm, customerId]);
});

router.delete('/rejectOrder/:customerId', async (req, res) => {
  const customerId = req.params.customerId;

  // Delete the comment from the database
  const query = 'DELETE FROM customer WHERE id = $1';
  await pool.query(query, [customerId]);

  res.json({ message: 'customer deleted successfully' });
});

module.exports = router;
 