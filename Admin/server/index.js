const express = require("express");
const app = express();
const cors = require("cors");

//middleware

app.use(cors());
app.use(express.json());

//login routes
app.use("/authentication", require("./routes/revmanAuth"));

//dashboard
app.use("/dashboard", require("./routes/dashboard"));

// add room routes
app.use("/room", require("./routes/room"));

// link curstomer server via room
app.use("/room", require("./routes/customer"));

// link curstomer server via food
app.use("/food", require("./routes/customer"));

// link food items
app.use("/food", require("./routes/food"));

//routes
app.listen(5000, () => {
  console.log(`Server is starting on port 5000`);
});